import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyDrawer());

class MyDrawer extends StatelessWidget {
  Widget build(BuildContext buildContext) {
    return MaterialApp(
      title: "My Drawer",
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
        brightness: Brightness.light,
      ),
      home: DrawerList(),
    );
  }
}

class DrawerList extends StatefulWidget {
  State createState() => _StateDrawerList();
}

class _StateDrawerList extends State<DrawerList> {
  Widget build(BuildContext buildContext) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Drawer Demo"),
        centerTitle: true,
      ),
      backgroundColor: Color(0xff3e5a7b),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text("Ankit Tiwari"),
                  accountEmail: Text("tiwariankit496@gmail.com"),
                  currentAccountPicture: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Center(
                      child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.network(
                            "https://www.freeiconspng.com/uploads/blue-user-icon-32.jpg",
                            fit: BoxFit.cover,
                          )),
                    ),
                  ),
                  otherAccountsPictures: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.pink,
                      child: Text("ABC"),
                      radius: 100.0,
                    )
                  ],
                ),
                ListTile(
                  title: Text("All inboxes"),
                  leading: Icon(Icons.inbox),
                  onTap: () {},
                ),
                ListTile(
                  title: Text("Social"),
                  leading: Icon(Icons.supervisor_account),
                ),
                ListTile(
                  title: Text("Promotion"),
                  leading: Icon(Icons.mode_edit),
                ),
                ListTile(
                  title: Text("Updates"),
                  leading: Icon(Icons.info_outline),
                ),
                ListTile(
                  title: Text("Forum"),
                  leading: Icon(Icons.forum),
                ),
                Container(
                  margin: EdgeInsets.only(right: 50.0),
                  child: Text("ALL LABELS"),
                ),
                ListTile(
                  title: Text("Starred"),
                  leading: Icon(Icons.star_border),
                ),
                ListTile(
                  title: Text("Snoozed"),
                  leading: Icon(Icons.access_time),
                ),
                ListTile(
                  title: Text("Important"),
                  leading: Icon(Icons.label_important),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
