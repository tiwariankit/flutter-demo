import './transaction.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "List Dynamic",
      debugShowCheckedModeBanner: false,
      home: ListDynamic(),
      theme: ThemeData(),
    );
  }
}
//coment
class ListDynamic extends StatefulWidget {
  @override
  _ListDynamicState createState() => _ListDynamicState();
}

class _ListDynamicState extends State<ListDynamic> {
  final nameC = TextEditingController();
  final emailC = TextEditingController();
  List<Transaction> transction = [];

  void addData(String name, String email) {
    setState(() {
      transction.add(Transaction(name: name, email: email));
      nameC.clear();
      emailC.clear();
    });
  }

  Widget textInputField(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (bctx) {
          return Container(
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextField(
                  controller: nameC,
                  decoration: InputDecoration(hintText: "Enter name"),
                ),
                TextField(
                  controller: emailC,
                  decoration: InputDecoration(hintText: "Enter email"),
                ),
                SizedBox(
                  height: 30.0,
                ),
                RaisedButton(
                  onPressed: () => addData(nameC.text, emailC.text),
                  child: Text("Add Data"),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff3e5b7a),
        title: Text("List Dynamic"),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.add), onPressed: () => textInputField(context))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Color(0xff3e5b7a),
        child: IconButton(
            icon: Icon(Icons.add), onPressed: () => textInputField(context)),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      backgroundColor: Color(0xfff3f8ff),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(transction[index].name),
              Text(transction[index].email),
            ],
          );
        },
        itemCount: transction.length,
      ),
    );
  }
}
