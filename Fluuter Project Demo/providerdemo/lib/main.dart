import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:providerdemo/ageModel.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => AgeModel(),
      child: MaterialApp(
        title: "Provider Demo",
        debugShowCheckedModeBanner: false,
        home: ProviderDemo(),
      ),
    );
  }
}

class ProviderDemo extends StatelessWidget {
  final agecontroller = TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AgeModel>(context);
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        title: Text("Provider Demo"),
      ),
      body: Center(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 50.0,
                width: 50.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: (provider.isEligible == null)
                      ? Colors.orangeAccent
                      : provider.isEligible ? Colors.green : Colors.red,
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                child: TextField(
                  controller: agecontroller,
                  decoration: InputDecoration(hintText: "enter age"),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                width: double.infinity,
                height: 35.0,
                child: RaisedButton(
                  color: Colors.blue,
                  onPressed: () {
                    if(agecontroller.text.trim().length==0){
                    final snackBar = SnackBar(
                      content: Text('please enter age!!!'),);
                    _scaffoldkey.currentState.showSnackBar(snackBar);
                    }else{
                      final age=int.parse(agecontroller.text.trim());
                      provider.checkAge(age);
                    }
                  },
                  child: Text('check'),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                child: Text(provider.message != null
                    ? provider.message
                    : "you havent enter any input"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
