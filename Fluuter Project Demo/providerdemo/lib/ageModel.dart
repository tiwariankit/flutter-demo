import 'package:flutter/material.dart';

class AgeModel extends ChangeNotifier{
  String _message="you havent enter any input";
  String _defmsg="you havent enter any input";
  bool _isEligible;

  void checkAge(int age){
    if(age>=18){
      _message="you are eligible";
      _isEligible=true;
      notifyListeners();
    }else{
      _message="you are no eligible";
      _isEligible=false;
      notifyListeners();
    }
  }

  String get message => _message;

  bool get isEligible => _isEligible;
  String get degmsh=>_defmsg;

}