import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(Myapp());

class Myapp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Button and Text Demo",
      home: AppBarHere(),
    );
  }
}

class AppBarHere extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Button and Text",
      home: MyHomePage(),
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.red,
        brightness: Brightness.light
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {
  String mytext="Hello";
  void changeText(){
    setState(() {
      mytext="Wecome";
    });
  }
  Widget getWidget(){
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Text(mytext),
            RaisedButton(
              child: Text("Click"),
              onPressed: changeText,
            )
          ],
        ),
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Button and Text Demo"),
        centerTitle: true,
        brightness: Brightness.dark,
      ),
      body: getWidget(),
    );
  }
}
