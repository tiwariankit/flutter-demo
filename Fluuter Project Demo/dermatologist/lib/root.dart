import 'dart:convert' as JSON;
import 'dart:io';

import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/ui/CommentView.dart';
import 'package:dermatologist/ui/DashBoardScreen.dart';
import 'package:dermatologist/ui/LoginScreen.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'model/AppSetting.dart';

class RootPage extends StatefulWidget {
  RootPage(this.userData);

  final UserData userData;

  @override
  State<StatefulWidget> createState() => new _RootPageState();
}

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  bool _isLoggedIn = false;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  var _load = false;
  BuildContext _context;

  @override
  void initState() {
    super.initState();
    checkAppSetting();
    firebaseCloudMessaging_Listeners();
  }

  checkAppSetting() async {
    if (await HttpHelper.isConnectedNew(context, checkAppSetting,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, checkAppSetting,
            barrierDismissible: true)) {
      getAppSetting();
    } else {
      setState(() {
        _isLoggedIn = widget.userData.loggedIn;
        authStatus =
            _isLoggedIn ? AuthStatus.LOGGED_IN : AuthStatus.NOT_LOGGED_IN;
      });
    }
  }

  getAppSetting() {
    HttpHelper.getAppSetting().then((response) {
      print(response.body);
      AppSetting appSetting =
          AppSetting.fromJson(JSON.jsonDecode(response.body)['data']);
      SharedPref sharedPref = new SharedPref();
      sharedPref.setAppSettingData(appSetting);
      setState(() {
        _isLoggedIn = widget.userData.loggedIn;
        authStatus =
            _isLoggedIn ? AuthStatus.LOGGED_IN : AuthStatus.NOT_LOGGED_IN;
      });
    }).catchError((error) {});
  }

  void firebaseCloudMessaging_Listeners() {
    var deviceToken, deviceType = "";
    if (Platform.isAndroid) {
      deviceType = "android";
    } else {
      deviceType = "ios";
    }
    final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.configure();


    Stream<String> fcmStream = _firebaseMessaging.onTokenRefresh;
    fcmStream.listen((token) {
      deviceToken = token;
      print("Root Token $deviceToken");
      SharedPref sharedPref = new SharedPref();
      iOS_Permission();
      sharedPref.notificationSettingData(deviceType, deviceToken);
    });

    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');

    var initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidRecieveLocalNotification);

    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    Future.delayed(Duration(milliseconds: 500), () {
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print(message["data"]['title']);
          displayNotification(message);
        },
        onBackgroundMessage: myBackgroundMessageHandler,
        onResume: (Map<String, dynamic> message) async {
          print("onResume ${message['data']['title']}");
          displayNotification(message);
        },
        onLaunch: (Map<String, dynamic> message) async {
          print("onLaunch ${message['data']['title']}");
          displayNotification(message);
        },
      );
    });
  }

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
      displayNotification(message);
    }
    // Or do other work.
  }

  Future onDidRecieveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => new CupertinoAlertDialog(
        title: new Text(title),
        content: new Text(body),
        actions: [
          CupertinoDialogAction(
            isDefaultAction: true,
            child: new Text('Ok'),
            onPressed: () async {
              Navigator.of(context, rootNavigator: true).pop();
              await Fluttertoast.showToast(
                  msg: "Notification Clicked",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  backgroundColor: Colors.black54,
                  textColor: Colors.white,
                  fontSize: 16.0);
            },
          ),
        ],
      ),
    );
  }

  Future displayNotification(Map<String, dynamic> message) async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'com.dermoto', 'flutterfcm', 'derma_app',
        importance: Importance.Max, priority: Priority.High);
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      message['data']['title'],
      message['data']['message'],
      platformChannelSpecifics,
      payload: message['data']['case_details'],
    );
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
//      CasesData casesData=CasesData.fromJson(json);
      debugPrint('notification payload: ' + payload);

      Navigator.push(
          _context,
          new MaterialPageRoute(
              builder: (BuildContext context) => new CommentViewPage(payload)));
    }

//    Navigator.push(
//        context,
//        new MaterialPageRoute(
//            builder: (BuildContext context) => new CaseDetails(data)));
    /*Navigator.push(
      context,
      new MaterialPageRoute(builder: (context) => new SecondScreen(payload)),
    );*/
  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      _firebaseMessaging.getToken().then((token) {}).catchError((error) {});
    });
  }

  Widget _buildWaitingScreen(Widget loadingIndicator) {
    return Scaffold(
      body: Align(
        child: loadingIndicator,
        alignment: FractionalOffset.center,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _context = context;
    Widget loadingIndicator = _load
        ? new Container()
        : new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          );
    switch (authStatus) {
      case AuthStatus.NOT_LOGGED_IN:
        return new LoginScreen(widget.userData);
        break;
      case AuthStatus.LOGGED_IN:
        if (_isLoggedIn) {
          //return new HomePage(widget.userData, _onSignedOut);
          return new DashBoardScreen(
              widget.userData.getUser(), widget.userData);
        } else
          return _buildWaitingScreen(loadingIndicator);
        break;
      default:
        return _buildWaitingScreen(loadingIndicator);
    }
  }
}
