import 'dart:async';

import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/root.dart';
import 'package:flutter/material.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  UserDataImp data = new UserDataImp();
  data.isLoggedIn().then((isLoggedIn) {
    Timer(Duration(seconds: 2), () {
      runApp(MyApp(data));
    });
  });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
//  String token;

//  MyApp(this.token);
  UserDataImp data;

  MyApp(this.data);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        builder: (context, child) {
          return MediaQuery(
            child: child,
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          );
        },
        title: 'Dermotologist App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            hintColor: Color.fromARGB(130, 183, 183, 183),
            unselectedWidgetColor: Color.fromARGB(200, 183, 183, 183),
            inputDecorationTheme: InputDecorationTheme(
                labelStyle:
                    TextStyle(color: Color.fromARGB(130, 183, 183, 183))),
            textTheme: TextTheme(
                title: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.w700)),
            primarySwatch: Colors.blue,
            fontFamily: "ProximaNova",
            scaffoldBackgroundColor: Colors.white,
            canvasColor: Colors.white,
            primaryTextTheme: TextTheme(
                title: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.w700))),
        home: new RootPage(this.data));
  }
}
