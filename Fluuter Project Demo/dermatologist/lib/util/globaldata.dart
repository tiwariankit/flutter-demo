import 'HexColor.dart';

class GlobalData {

  static bool isEmailValid(String emailId)
  {
    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);

    return regExp.hasMatch(emailId);
  }

  static colorSet(Future<String> color) {
    color.then((value) {
      HexColor(value);
    });
  }


}
