import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static final _instance = DBProvider._internal();
  static DBProvider get = _instance;
  bool isInitialized = false;
  Database _db;

  DBProvider._internal();

  DBProvider._privateConstructor();

  static final DBProvider instance = DBProvider._privateConstructor();

  static final _databaseName = "dermatologist.db";
  static final _databaseVersion = 1;

  static final case_list = 'case_list';
  static final table_prescription_data = 'table_prescription_data';
  static final table_cmsData = 'table_cmsData';
  static final table_cmsListData = 'table_cmsListData';

  //CAse Field.
  static final admin_id = 'admin_id';
  static final case_id = 'case_id';
  static final id = 'id';
  static final user_id = 'user_id';
  static final problem_occur = 'problem_occur';
  static final Name = 'Name';
  static final contact = 'contact';
  static final sex = 'sex';
  static final description = 'description';
  static final transaction_id = 'transaction_id';
  static final amount = 'amount';
  static final status = 'status';
  static final date = 'date';
  static final time = 'time';
  static final photos = 'photos';
  static final is_offline = 'is_offline';

  //PrescriptionData

  static final pres_admin_id = 'pres_admin_id';
  static final pres_case_id = 'pres_case_id';
  static final pres_user_id = 'pres_user_id';
  static final pres_data = 'pres_data';

  //CMS DATA

  static final cms_admin_id = 'cms_admin_id';
  static final cms_user_id = 'cms_user_id';
  static final cms_id = 'cms_id';
  static final cms_title = 'cms_title';
  static final cms_data = 'cms_data';

  static final cms_listadmin_id = 'cms_admin_id';
  static final cms_listuser_id = 'cms_user_id';
  static final cms_listid = 'cms_id';
  static final cms_listtitle = 'cms_title';

  Future<Database> get database async {
    if (_db != null) return _db;
    // lazily instantiate the db the first time it is accessed
    _db = await _init();
    return _db;
  }

  Future _init() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, _databaseName);
    return await openDatabase(path, version: _databaseVersion, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('''
          CREATE TABLE $case_list (
          $id TEXT NOT NULL,
          $admin_id TEXT NOT NULL,
            $case_id TEXT NOT NULL,
            $photos
             TEXT NOT NULL,
            $user_id TEXT NOT NULL,
            $problem_occur TEXT NOT NULL,
            $Name TEXT NOT NULL,
            $contact TEXT NOT NULL,
            $sex TEXT NOT NULL,
            $description TEXT NOT NULL,
            $transaction_id TEXT NOT NULL,
            $amount TEXT NOT NULL,
            $status TEXT NOT NULL,
            $date TEXT NOT NULL,
            $time TEXT NOT NULL,
            $is_offline TEXT NOT NULL
            )''');

      await db.execute('''
          CREATE TABLE $table_cmsData (
          $cms_admin_id TEXT NOT NULL,
          $cms_user_id TEXT NOT NULL,
          $cms_title TEXT NOT NULL,
            $cms_id TEXT NOT NULL,
            $cms_data TEXT NOT NULL
            )''');

      await db.execute('''
          CREATE TABLE $table_prescription_data (
          $pres_admin_id TEXT NOT NULL,
          $pres_user_id TEXT NOT NULL,
          $pres_case_id TEXT NOT NULL,
            $pres_data TEXT NOT NULL
            )''');

      await db.execute('''
          CREATE TABLE $table_cmsListData (
          $cms_listid TEXT NOT NULL,
          $cms_listadmin_id TEXT NOT NULL,
          $cms_listtitle TEXT NOT NULL
            )''');
    });
  }
}
