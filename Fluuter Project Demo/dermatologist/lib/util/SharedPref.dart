import 'package:dermatologist/model/AppSetting.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPref {
  void setUserData(UserModel model) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("firstName", model.firstname);
    sharedPreferences.setString("lastName", model.lastname);
    sharedPreferences.setString("email", model.email);
    sharedPreferences.setString("user_id", model.user_id);
    sharedPreferences.setString("image", model.image);
    sharedPreferences.commit();
//    getUserData();
  }

  void notificationSettingData(String deviceType, String deviceToken) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("deviceType", deviceType);
    sharedPreferences.setString("deviceToken", deviceToken);
    sharedPreferences.commit();
//    getUserData();
  }

  void getUserData() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    print(sharedPreferences.getString("firstName"));
    print(sharedPreferences.getString("lastName"));
    print(sharedPreferences.getString("email"));
    print(sharedPreferences.getString("user_id"));
    print(sharedPreferences.getString("image"));
  }

  void setAppSettingData(AppSetting model) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("id", model.id);
    sharedPreferences.setString("admin_id", model.admin_id);
    sharedPreferences.setString("logo", model.logo);
    sharedPreferences.setString("name", model.name);
    sharedPreferences.setString("color", model.color);
    sharedPreferences.setString("foot_color", model.footer_color);
    sharedPreferences.setString("header_text_color", model.header_text_color);
    sharedPreferences.setString("footer_text_color", model.footer_text_color);
    sharedPreferences.setString("address", model.address);
    sharedPreferences.setString("latitute", model.latitute);
    sharedPreferences.setString("longitute", model.longitute);
    sharedPreferences.commit();
//    getUserData();

//    getTopBackColor();
  }

  Future<String> getTopBackColor() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    return sharedPreferences.getString("color");
  }

  Future<String> getBottomBackColor() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("foot_color");
  }

  Future<String> getBottomTextColor() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("footer_text_color");
  }

  Future<String> getTopTextColor() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("header_text_color");
  }

  Future<String> getDeviceType() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("deviceType");
  }

  Future<String> getDeviceToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("deviceToken");
  }

  Future<String> getUserId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("user_id");
  }

  Future<String> getTokenizationKey() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("tokenizationKey");
  }

  Future<String> getCompanyName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("companyName");
  }

  Future<String> getFullName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return "${sharedPreferences.getString("firstName")} ${sharedPreferences.getString("lastName")}";
  }

  Future<String> getFirstName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return "${sharedPreferences.getString("firstName")}";
  }

  Future<String> getLastName() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return "${sharedPreferences.getString("lastName")}";
  }


  void setPaymentData(String tokenizationKey,String companyName) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString("tokenizationKey", tokenizationKey);
    sharedPreferences.setString("companyName", companyName);
    sharedPreferences.commit();
//    getUserData();
  }



  static Future<String> getAdminId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("admin_id");
  }
}
