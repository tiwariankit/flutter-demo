import 'dart:async';
import 'dart:io';
import 'dart:io'
    show
        File,
        HttpClient,
        HttpClientResponse,
        InternetAddress,
        Platform,
        SocketException;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'SharedPref.dart';

class HttpHelper {
  static String mainUrl = "https://dermatologist-new.fmv.cc/api/";
  static String url = mainUrl + "User";
  static String cases = mainUrl + "Cases";
  static String cms = mainUrl + "Cms/getCmsDetail";
  static String cmsListing = mainUrl + "Cms/getCms";
  static String prescription = mainUrl + "Prescription";
  static String clinicList = mainUrl + "Clinic";
  static String message = mainUrl + "Message";
  static String braintreePayment = mainUrl + "transaction/new_transaction";
  static String getPaymentDetail = mainUrl + "Settings/getPaymentKeys";
//  static String getAppSettingUrl = mainUrl + "Settings/getDoctorAppSettings";
  static String getAppSettingUrl = mainUrl + "Settings/getDoctorAppSettings_81";


  static String registration = "/registration";
  static String login = "/loginv2";
  static String forgotPasswordAPi = "/forgotPassword";
  static String addCases = "/addCase";
  static String getCasesData = "/getCases";
  static String DOCTORLIST = "/getAllDoctors";
  static String DoctorDetails = "/getDoctorDetail";
  static String getPrescrition = "/getPrescriptions";
  static String getClinicList = "/getClinic";
  static String updateProfileAPI = "/updateProfile";
  static String sendMessage = message + "/sendMessage";
  static String getMessage = message + "/getMessageDetail";

  static String admin_id;

  static Future<http.Response> doRegistration(String firstname, String lastname,
      String email, String password, String confirm_password) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };

    var data = {
      'firstname': firstname,
      'lastname': lastname,
      'email': email,
      'password': password,
      'confirm_password': confirm_password,
      'admin_id': admin_id
    };

    return http.post(url + registration, headers: headers, body: data);
  }

  static Future<http.Response> userLogin(
      String email, String password, String deviceType, String deviceToken) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {
      'email': email,
      'password': password,
      'admin_id': admin_id,
      'device_type': deviceType,
      'device_token': deviceToken
    };

    return http.post(url + login, headers: headers, body: data);
  }

  static Future<http.Response> forgotPassword(String email) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'email': email, 'admin_id': admin_id};

    return http.post(url + forgotPasswordAPi, headers: headers, body: data);
  }

  static Future<http.Response> sendMessagemethod(
      String caseId, String message) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'case_id': caseId, 'description': message};

    return http.post(sendMessage, headers: headers, body: data);
  }

  static Future<http.Response> getMessageMethod(String caseId) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'case_id': caseId};

    return http.post(getMessage, headers: headers, body: data);
  }

  static Future<http.Response> doctorDetails(String doctor_id) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'doctor_id': doctor_id, 'admin_id': admin_id};

    return http.post(url + DoctorDetails, headers: headers, body: data);
  }

  static Future<http.Response> skinGuide(String id) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'id': id, 'admin_id': admin_id};

    return http.post(cms, headers: headers, body: data);
  }

  static Future<http.Response> getCmsListing() {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'admin_id': admin_id};

    return http.post(cmsListing, headers: headers, body: data);
  }

  static Future<http.Response> getAppSetting() {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };

    return http.get(getAppSettingUrl, headers: headers);
  }

  static Future<http.Response> ClinicList(String latitute, String longitute) {
    var data = {
      'latitute': latitute,
      'longitute': longitute,
      'admin_id': admin_id
    };

    return http.post(clinicList + getClinicList, body: data);
  }

  static Future<http.Response> getPrescription(String case_id) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'case_id': case_id, 'admin_id': admin_id};

    return http.post(prescription + getPrescrition,
        headers: headers, body: data);
  }

  static Future<http.Response> getCases(String id) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'user_id': id, 'admin_id': admin_id};

    return http.post(cases + getCasesData, headers: headers, body: data);
  }

  static Future<http.Response> doctorList() {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };

    return http.get(url + DOCTORLIST, headers: headers);
  }

  static Future<http.Response> checkoutAPI(String amount, String nonce,
      String userid, String paymenType, String full_response) {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };

    var data = {
      'amount': amount,
      'payment_method_nonce': nonce,
      'user_id': userid,
      'admin_id': admin_id,
      'payment_term': paymenType,
      'full_response': full_response
    };
    print("SEND DATA $data");
    return http.post(braintreePayment, body: data, headers: headers);
  }

  static Future<http.Response> getPaymentKey() {
    var headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    };
    var data = {'admin_id': admin_id};

    return http.post(getPaymentDetail, headers: headers, body: data);
  }

  static isConnectedNew(BuildContext context, Function retryMethod,
      {bool barrierDismissible}) async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
//      bool dismiss = barrierDismissible == null;
//      showDialog(
//          barrierDismissible: dismiss,
//          context: context,
//          builder: (BuildContext context) {
//            return AlertDialog(
//              title: new Text("No Interne"),
//              content: new Text(
//                  "Please check your internet connection and try again!"),
//              actions: <Widget>[
//                !dismiss
//                    ? SizedBox()
//                    : FlatButton(
//                        onPressed: () {
//                          Navigator.of(context).pop();
//                        },
//                        child: Text("Close"),
//                      ),
//                FlatButton(
//                  onPressed: () {
//                    Navigator.of(context).pop();
//                    retryMethod();
//                  },
//                  child: Text("Retry"),
//                ),
//              ],
//            );
//          });
      return false;
    }
//    Connectivity()
//        .onConnectivityChanged
//        .listen((ConnectivityResult connectivityResult) {
//      if (connectivityResult == ConnectivityResult.mobile ||
//          connectivityResult == ConnectivityResult.wifi) {
//        return true;
//      } else {
//
//      }
//    });
  }

  static void getAdminId() {
    SharedPref.getAdminId().then((id) {
      admin_id = id;
    });
  }
}
