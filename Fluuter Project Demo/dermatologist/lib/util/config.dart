import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

const PaypalConfig = {
  "clientId": "Ad610_aWT2coDHYoIZ4h6bxCPiamEle8iZCCXDuQOdn3YbZxXimFarmRM4gQ4bYD5GgoN_CPl3mfz2gM",
  "secret": "EM3xI8yU8uKdiBWg6SKNKlNttuYrV18eF_QWY76EjyCpMZr5zxLAso5lTQP1LUtDI9OHgtDgh4oL2Cif",
  "production": false,
  "paymentMethodId": "paypal",
  "enabled": true,
  "returnUrl": "https://dermatologist-new.fmv.cc/api/Settings/returnUrl",
  "cancelUrl": "https://dermatologist-new.fmv.cc/api/Settings/cancelUrl",
};
const kGrey200 = Color(0xFFEEEEEE);
Widget kLoadingWidget(context) => Center(
  child: SpinKitFadingCube(
    color: Theme.of(context).primaryColor,
    size: 30.0,
  ),
);
const kAdvanceConfig = {
  "DefaultLanguage": "en",
  "DefaultCurrency": {
    "symbol": "\$",
    "decimalDigits": 2,
    "symbolBeforeTheNumber": true,
    "currency": "USD"
  },
  "IsRequiredLogin": false,
  "GuestCheckout": true,
  "EnableShipping": false,
  "EnableAddress": false,
  "EnableReview": true,
  "GridCount": 3,
  "EnablePointReward": true,
  "DefaultPhoneISOCode": "+84",
  "DefaultCountryISOCode": "VN",
  "EnableRating": true,
  "EnableSmartChat": true,
  "hideOutOfStock": true,
  'allowSearchingAddress': true,
  "isCaching": false,
  "OnBoardOnlyShowFirstTime": true,
  "EnableConfigurableProduct": false, //for magento
  "EnableAttributesConfigurableProduct": ["color", "size"], //for magento
  "EnableAttributesLabelConfigurableProduct": ["color", "size"], //for magento,
  "EnableAdvertisement": true,
  "Currencies": [
    {"symbol": "\$", "decimalDigits": 2, "symbolBeforeTheNumber": true, "currency": "USD"},
    {"symbol": "đ", "decimalDigits": 2, "symbolBeforeTheNumber": true, "currency": "VND"}
  ]
};