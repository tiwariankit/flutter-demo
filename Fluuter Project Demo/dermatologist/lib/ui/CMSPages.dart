import 'dart:convert' as JSON;

import 'package:dermatologist/model/CmsDetailData.dart';
import 'package:dermatologist/model/CasesData.dart';
import 'package:dermatologist/util/DBProvider.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:url_launcher/url_launcher.dart';

class CMSPages extends StatefulWidget {
  String id;

  CMSPages(this.id);

  @override
  _MyCMSPages createState() => _MyCMSPages();
}

class _MyCMSPages extends State<CMSPages> {
  List<CasesData> caseList = new List();
  var isLoadData = false;
  List<CMSData> list = new List();
  String title = "";
  bool _load = false;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF",
      app_id = "",
      user_id = "";

  @override
  void initState() {
    // TODO: implement initState

    setPrefrenceData();
    _SkinGuide();
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();

    SharedPref.getAdminId().then((id) {
      setState(() {
        app_id = id;
      });
    });

    _sharedPref.getUserId().then((id) {
      setState(() {
        user_id = id;
      });
    });

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = !_load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

//    if (widget.id == "29") {
//      setState(() {
//        title = "FAQs";
//      });
//    } else if (widget.id == "31") {
//      setState(() {
//        title = "About us";
//      });
//    } else if (widget.id == "35") {
//      setState(() {
//        title = "Contact Us";
//      });
//    } else if (widget.id == "33") {
//      setState(() {
//        title = "Privacy Policy";
//      });
//    } else if (widget.id == "34") {
//      setState(() {
//        title = "Instruction";
//      });
//    } else if (widget.id == "28") {
//      setState(() {
//        title = "Skin Guide";
//      });
//    }

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: HexColor(topBackColor),
        actions: <Widget>[
          Container(
            color: HexColor(topBackColor),
          )
        ],
        leading: new IconButton(
          icon: new Icon(Icons.chevron_left, color: HexColor(topTextColor)),
          onPressed: () => Navigator.of(context).pop(""),
        ),
        title: Text(
          title,
          style: TextStyle(color: HexColor(topTextColor)),
        ),
      ),
      body: _load
          ? ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
              scrollDirection: Axis.vertical,
              itemCount: list.length,
              itemBuilder: (context, i) {
                // If you've reached at the end of the available word pairs...
                return _buildRow(list[i], i);
              },
            )
          : Align(
              child: loadingIndicator,
              alignment: FractionalOffset.center,
            ),
    );
  }

  Widget _buildRow(CMSData data, int index) {
    return new ListTile(
      title: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            HtmlWidget(
              data.content,
              hyperlinkColor: Theme.of(context)
                  .primaryColor
                  .withOpacity(0.9),
              textStyle:
              Theme.of(context).textTheme.body1.copyWith(
                fontSize: 13.0,
                height: 1.4,
              ),
            )
          ],
        ),
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _checkCMSData() async {
    Database db = await DBProvider.instance.database;

    List<Map> results = await db.query(DBProvider.table_cmsData,
        where: '${DBProvider.cms_admin_id} = ? AND ${DBProvider.cms_id} =  ?',
        whereArgs: [app_id, widget.id]);

    if (results.length > 0) {
      _deleteCmsData();
    } else {
      _insertCmsData();
    }
  }

  _getOfflineCMSDATA() async {
    Database db = await DBProvider.instance.database;

    List<Map> results = await db.query(DBProvider.table_cmsData,
        where: '${DBProvider.cms_admin_id} = ? AND ${DBProvider.cms_id} =  ?',
        whereArgs: [app_id, widget.id]);
    List<CMSData> tmp_cmsList = new List();
    if (results.length > 0)
    {
      tmp_cmsList.add(new CMSData(results[0][DBProvider.cms_id],
          results[0][DBProvider.cms_title], results[0][DBProvider.cms_data]));
      setState(() {
        _load = true;
        title=results[0][DBProvider.cms_title];
        this.list = tmp_cmsList;
      });
    } else {
      setState(() {
        _load = true;
      });
    }
  }

  _deleteCmsData() async {
    Database db = await DBProvider.instance.database;

    int data = await db.rawDelete(
        'DELETE FROM ${DBProvider.table_cmsData} WHERE ${DBProvider.cms_admin_id} = ${app_id} AND ${DBProvider.cms_id} = ${widget.id}');

    print(data);
    _insertCmsData();
  }

  _insertCmsData() async {
    Database db = await DBProvider.instance.database;

    Map<String, dynamic> row = {
      DBProvider.cms_admin_id: app_id,
      DBProvider.cms_id: widget.id,
      DBProvider.cms_user_id: user_id,
      DBProvider.cms_title: list[0].title,
      DBProvider.cms_data: list[0].content
    };
    await db.insert(DBProvider.table_cmsData, row);
  }

  _SkinGuide() async {
    if (await HttpHelper.isConnectedNew(context, _SkinGuide,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, _SkinGuide,
            barrierDismissible: true)) {
      HttpHelper.skinGuide(widget.id).then((response) {
        print(response.body);
        var l =
            JSON.jsonDecode(response.body)['data'].cast<Map<String, dynamic>>();
        List<CMSData> list =
            l.map<CMSData>((json) => CMSData.fromJson(json)).toList();

        setState(() {
          _load = true;
          this.list = list;
          title=list[0].title;
          _checkCMSData();
        });
      }).catchError((error) {});
    } else {
      _getOfflineCMSDATA();
    }
  }
}
