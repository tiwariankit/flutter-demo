import 'package:dermatologist/model/ClinicData.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class ClinicDetail extends StatefulWidget {
  ClinicData clinicData;
  ClinicDetail(this.clinicData);
  @override
  _ClinicDetailState createState() => _ClinicDetailState();
}

class _ClinicDetailState extends State<ClinicDetail> {
  var isLoadData = false;
  int _currValue = 0;
  bool isDays = true, isWeeks = false, isMonth = false, isYears = false;
  String text = "";

  GoogleMapController mapController;
  Set<Marker> markers = Set();
  bool isLatLong = false;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
  }

  @override
  void initState() {
    // TODO: implement initState
    setPrefrenceData();
    addMarkerData();
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            backgroundColor: HexColor(topBackColor),
            actions: <Widget>[
              Container(
                color: HexColor(topBackColor),
              )
            ],
            leading: new IconButton(
              icon: new Icon(Icons.chevron_left, color: HexColor(topTextColor)),
              onPressed: () => Navigator.of(context).pop("Hi Helloooo"),
            )),
        body: Container(
          child: ListView(
            children: <Widget>[
              new Stack(
                children: <Widget>[
                  new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                      width: double.infinity,
                      height: 250,
                      child: isLatLong
                          ? GoogleMap(
                              markers: markers,
                              rotateGesturesEnabled: true,
                              scrollGesturesEnabled: true,
                              tiltGesturesEnabled: true,
                              onMapCreated: _onMapCreated,
                              initialCameraPosition: CameraPosition(
                                target: new LatLng(
                                    double.parse(widget.clinicData.latitute),
                                    double.parse(widget.clinicData.longitute)),
                                zoom: 11.5,
                              ),
                            )
                          : new Container(
                              child: Text("Invalid Latlong"),
                            )),
                  isLatLong
                      ? Padding(
                          padding: const EdgeInsets.only(
                              bottom: 20, left: 10, right: 10),
                          child: Container(
                            alignment: Alignment.bottomRight,
                            height: 50,
                            child: RaisedButton(
                              color: HexColor(topBackColor),
                              shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(8.0)),
                              child: Text(
                                "OPEN IN A MAPS APP",
                                style: TextStyle(color: HexColor(topTextColor)),
                              ),
                              onPressed: () {
                                _launchMapsUrl(
                                    double.parse(widget.clinicData.latitute),
                                    double.parse(widget.clinicData.longitute));
                              },
                            ),
                          ),
                        )
                      : new Container()
                ],
              ),
              new Container(
                margin: EdgeInsets.fromLTRB(20, 10, 20, 0.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.clinicData.name,
                        style: TextStyle(fontSize: 20, color: HexColor(topBackColor))),
                    new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10, 20, 0),
                      height: 1,
                      width: double.infinity,
                      color: HexColor(topBackColor),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0.0, 8, 0.0, 8),
                      child: Text("Address",
                          style: TextStyle(color: Colors.grey, fontSize: 17)),
                    ),
                    Text(
                      widget.clinicData.location,
                      style: TextStyle(fontSize: 15, color: Colors.black),
                      overflow: TextOverflow.clip,
                    ),
                    new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10, 20, 0),
                      height: 1,
                      width: double.infinity,
                      color: HexColor(topBackColor),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0.0, 8, 0.0, 8),
                      child: Text("Website",
                          style: TextStyle(color: Colors.grey, fontSize: 17)),
                    ),
                    new GestureDetector(
                      onTap: () => {_launchURL(widget.clinicData.website)},
                      child: Text(
                        widget.clinicData.website,
                        style: TextStyle(fontSize: 15, color: HexColor(topBackColor)),
                        overflow: TextOverflow.clip,
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10, 20, 0),
                      height: 1,
                      width: double.infinity,
                      color: HexColor(topBackColor),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0.0, 8, 0.0, 8),
                      child: Text("Phone",
                          style: TextStyle(color: Colors.grey, fontSize: 17)),
                    ),
                    InkWell(
                      onTap: () =>
                          _launchCaller("tel:${widget.clinicData.phone}"),
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.call,
                            color: HexColor(topBackColor),
                            size: 36,
                          ),
                          Text(
                            widget.clinicData.phone,
                            style: TextStyle(fontSize: 18, color: Colors.black),
                            overflow: TextOverflow.clip,
                          )
                        ],
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10, 20, 0),
                      height: 1,
                      width: double.infinity,
                      color: HexColor(topBackColor),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0.0, 8, 0.0, 8),
                      child: Text("Specialties : ",
                          style: TextStyle(color: Colors.grey, fontSize: 17)),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10.0, 0, 0.0, 8),
                      child: _getSpecialites(widget.clinicData.specialities),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  _launchURL(String website) async {
    if (await canLaunch(website)) {
      await launch(website);
    } else {
      throw 'Could not launch $website';
    }
  }

  _launchCaller(String number) async {
    if (await canLaunch(number)) {
      await launch(number);
    } else {
      throw 'Could not launch $number';
    }
  }

  Widget _getSpecialites(String name) {
    if (name == "1") {
      return Text("Dermatology",
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w700));
    } else if (name == "2") {
      return Text("Pediatrics",
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w700));
    } else if (name == "3") {
      return Text("Pharmacies",
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w700));
    }
  }

  void addMarkerData() async {
    if (widget.clinicData.latitute != null &&
        widget.clinicData.longitute != null) {
      Marker resultMarker = await Marker(
        markerId: MarkerId(widget.clinicData.id),
        infoWindow: InfoWindow(
            title: "${widget.clinicData.name}",
            snippet: "${widget.clinicData.name}"),
        position: LatLng(double.parse(widget.clinicData.latitute),
            double.parse(widget.clinicData.longitute)),
      );
// Add it to Set

      setState(() {
        markers.add(resultMarker);
        isLatLong = true;
      });
    } else {
      setState(() {
        isLatLong = false;
      });
    }
  }

  void _launchMapsUrl(double lat, double lon) async {
    final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
