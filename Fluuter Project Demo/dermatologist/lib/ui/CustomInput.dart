import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';


class CustomInput extends StatelessWidget {
  final String inputText;
  final IconData inputIcon;
  final TextInputType keyboardType;
  final TextEditingController controllertext;
  final bool enabled;
 final FocusNode textFocus;
 final int maxLength;
 final bool obscureText;
 //final GestureTapCallback onTab;
  //final GestureTapCallback onTap;

  const CustomInput(
      {Key key, this.inputText, this.inputIcon,this.keyboardType,this.controllertext,this.enabled,this.textFocus,this.maxLength,this.obscureText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextField(
        obscureText: obscureText,
        controller: controllertext,
       // initialValue: inputText,
        enabled: enabled,
        maxLength: maxLength,
        decoration: InputDecoration(labelText: inputText,prefixIcon: Icon(inputIcon,color: AppColors.blue),focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.blue)),hintMaxLines: 1,labelStyle: TextStyle(fontWeight: FontWeight.bold)),
        cursorColor: AppColors.blue,
        keyboardType: keyboardType,
      ),

    );
  }
}
