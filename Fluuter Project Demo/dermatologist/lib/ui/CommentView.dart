import 'dart:async';
import 'dart:convert';
import 'dart:convert' as JSON;
import 'dart:io';

import 'package:async/async.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dermatologist/model/CommentModel.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';

import 'full_imageview.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class CommentViewPage extends StatefulWidget {
  String id;

  CommentViewPage(this.id);

  @override
  _CommentViewPageState createState() => _CommentViewPageState();
}

class _CommentViewPageState extends State<CommentViewPage> {
  final TextEditingController _textEditingController =
      new TextEditingController();
  bool _isComposingMessage = false;
  bool _load = false;
  List<CommentModel> _listComment = new List();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController _scrollController = new ScrollController();
  File image;
  bool _isFromCamera = false;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF",
      app_id = "",
      user_id = "";

  BuildContext context;
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setPrefrenceData();
    _getMessageListData();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();

    _sharedPref.getUserId().then((id) {
      setState(() {
        user_id = id;
      });
    });
    SharedPref.getAdminId().then((id) {
      setState(() {
        app_id = id;
      });
    });

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {

    this.context = context;


    void _onRefresh() async{
      await Future.delayed(Duration(milliseconds: 1000));
      _refreshController.refreshCompleted();
    }


    Widget loadingIndicator = _load
        ? new Container()
        : new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          );

    return Scaffold(
      resizeToAvoidBottomPadding: true,
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: HexColor(topBackColor),
        title: Text(
          "Comments",
          style: TextStyle(color: HexColor(topTextColor)),
        ),
      ),
      body: SafeArea(
        top: true,
        bottom: true,
        child: Stack(
          children: <Widget>[

            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, 55),
              child: _load
                  ? _listComment.length > 0
                  ? new Container(
                child: SmartRefresher(
                  enablePullDown: true,
                  enablePullUp: false,
                  header: WaterDropHeader(),
                  footer: CustomFooter(
                    builder: (BuildContext context,LoadStatus mode){
                      Widget body ;
                      if(mode==LoadStatus.idle){
                        body =  Text("pull up load");
                      }
                      else if(mode==LoadStatus.loading){
                        body =  CupertinoActivityIndicator();
                      }
                      else if(mode == LoadStatus.failed){
                        body = Text("Load Failed!Click retry!");
                      }
                      else if(mode == LoadStatus.canLoading){
                        body = Text("release to load more");
                      }
                      else{
                        body = Text("No more Data");
                      }
                      return Container(
                        height: 55.0,
                        child: Center(child:body),
                      );
                    },
                  ),
                  controller: _refreshController,
                  onRefresh: _onRefresh,
                  onLoading: _getMessageListData,
                  child: ListView.separated(
                    separatorBuilder: (context, index) => Divider(
                      color: Colors.black,
                    ),
                    controller: _scrollController,
                    scrollDirection: Axis.vertical,
                    itemCount: _listComment.length,
                    itemBuilder: (context, i) {
                      return _buildRow(_listComment[i], i);
                    },
                  ),
                ),
              )
                  : new Container(
                child: Text(""),
                margin: EdgeInsets.fromLTRB(0.0, 50, 0.0, 10),
              )
                  : Align(
                child: loadingIndicator,
                alignment: FractionalOffset.center,
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: new Container(
                height: 54,
                decoration:
                new BoxDecoration(color: Theme.of(context).cardColor),
                child: Column(
                  children: <Widget>[
                    new Divider(height: 1.0),
                    _buildTextComposer(context),
                    new Divider(height: 1.0)
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void openImageView(String ans) {
    print(ans);

    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new FullImageView(ans)));
  }

  Widget _buildRow(CommentModel data, int index) {
    return new ListTile(
      title: Container(
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(new Radius.circular(0))),
        child: new Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  data.profile_image.isNotEmpty && data.profile_image != null
                      ? new Image.network(
                          data.profile_image,
                          height: 30,
                          width: 30,
                        )
                      : new Image.asset(
                          "assets/images/boy.png",
                          height: 30,
                          width: 30,
                        ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5),
                    child: Text(
                      data.sender_name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 35, right: 10),
                    child: Text(
                      data.description,
                      maxLines: 10,
                    ),
                  ),
                  data.images.isNotEmpty && data.images != null
                      ? new Container(
                          margin: const EdgeInsets.only(
                              top: 5, left: 30, right: 10),
                          decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 7.0,
                                  color: Colors.black.withOpacity(0.3),
                                ),
                              ],
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: Colors.white),
                          width: 100,
                          height: 100,
                          child: InkWell(
                            onTap: () => {openImageView(data.images)},
                            child: ClipRRect(
                              borderRadius: new BorderRadius.circular(5.0),
                              child: Stack(
                                children: <Widget>[
                                  new CachedNetworkImage(
                                    imageUrl: data.images.trim(),
                                    height: 100,
                                    width: 100,
                                    fit: BoxFit.fill,
                                  )
                                ],
                              ),
                            ),
                          ),
                        )
                      : new Container(),
                  Align(
                      alignment: Alignment.topLeft,
                      child: InkWell(
                        onTap: () => {},
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 35,
                              right: 10,
                              top: 5
                          ),
                          child: Text(
                            data.message_time,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                        ),
                      )),
                ],
              ),
            ],
          ),
          padding: EdgeInsets.all(5),
        ),
      ),
    );
  }

  _getMessageListData() async {
    if (await HttpHelper.isConnectedNew(context, _sendMEssageApi,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, _sendMEssageApi,
            barrierDismissible: true)) {
      await HttpHelper.getMessageMethod(widget.id).then((response) {
        _refreshController.loadComplete();
        print(response.body);
        List<CommentModel> caselist = new List();
        var jsonParse = json.decode(response.body);
        if (jsonParse['status']) {
          var l = JSON
              .jsonDecode(response.body)['data']
              .cast<Map<String, dynamic>>();
          print(response.body);
          List<CommentModel> caselist = l
              .map<CommentModel>((json) => CommentModel.fromJson(json))
              .toList();
          setState(() {
            _load = true;
            _listComment = caselist;
          });

          print(_listComment.length);
        } else {
          buildSnackbar(jsonParse['message']);

          setState(() {
            _load = true;
            _listComment = caselist;
          });
        }
      }).catchError((error) {
        setState(() {
          _load = true;
        });
      });
    } else {
      buildSnackbar("Please connect to internet");
      setState(() {
        _load = true;
      });
    }
  }

  Widget _buildTextComposer(BuildContext context) {
    return new IconTheme(
        data: new IconThemeData(
          color: _isComposingMessage
              ? Theme.of(context).accentColor
              : Theme.of(context).disabledColor,
        ),
        child: new Container(
          margin: const EdgeInsets.symmetric(horizontal: 8.0),
          child: new Row(
            children: <Widget>[
              new Container(
                margin: new EdgeInsets.symmetric(horizontal: 4.0),
                child: new IconButton(
                    icon: new Icon(
                      Icons.add,
                      color: Theme.of(context).accentColor,
                    ),
                    onPressed: () async {
                      _optionsDialogBox(context);
                    }),
              ),
              new Flexible(
                child: new TextField(
                  controller: _textEditingController,
                  onChanged: (String messageText) {
                    setState(() {
                      _isComposingMessage = messageText.length > 0;
                    });
                  },
//                  onSubmitted: _textMessageSubmitted,
                  decoration:
                      new InputDecoration.collapsed(hintText: "Send a message"),
                ),
              ),
              new Container(
                margin: const EdgeInsets.symmetric(horizontal: 0.0),
                child: Theme.of(context).platform == TargetPlatform.iOS
                    ? getIOSSendButton(context)
                    : getDefaultSendButton(context),
              ),
            ],
          ),
        ));
  }

  CupertinoButton getIOSSendButton(BuildContext context) {
    return new CupertinoButton(
      child: new Text("Send"),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text)
          : null,
    );
  }

  IconButton getDefaultSendButton(BuildContext context) {
    return new IconButton(
      icon: new Icon(Icons.send),
      onPressed: _isComposingMessage
          ? () => _textMessageSubmitted(_textEditingController.text)
          : null,
    );
  }

  Future<Null> _textMessageSubmitted(String text) async {
    _textEditingController.clear();

    setState(() {
      _isComposingMessage = false;
    });

    if(text != "") {
      _sendMEssageApi(text, null);
    }
  }

  _sendMEssageApi(String message, File image) async {
    if (await HttpHelper.isConnectedNew(context, _sendMEssageApi,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, _sendMEssageApi,
            barrierDismissible: true)) {
      var uri = Uri.parse(HttpHelper.sendMessage);

      var request = new http.MultipartRequest("POST", uri);
      var data = {
        'case_id': widget.id,
        'description': message,
      };

      request.fields.addAll(data);

      if (image != null) {
        var stream =
            new http.ByteStream(DelegatingStream.typed(image.openRead()));
        var length = await image.length();

        var multipartFile = new http.MultipartFile('images', stream, length,
            filename: basename(image.path));
        request.files.add(multipartFile);
      }
      var response = await request.send();
      print(response.toString());
      response.stream.transform(utf8.decoder).listen((value) {
        var data = json.decode(value);
        print(data['id']);
        if (data['status']) {
          var l = data['data'].cast<Map<String, dynamic>>();
          print(data);
          List<CommentModel> caselist = l
              .map<CommentModel>((json) => CommentModel.fromJson(json))
              .toList();

          setState(() {
            _listComment.insert(0, caselist.first);
            _scrollController.jumpTo(0);
//            _listComment.addAll(caselist);
            _load = true;
          });
        } else {
          setState(() {
            _load = true;
          });
          buildSnackbar(data['message']);
        }
        return Future<String>.value(value);
      }).onError((error) {
        setState(() {
          _load = true;
        });
        print("Error: $error");
        return Future<String>.value("Error $error");
      });
    } else {
      buildSnackbar("Please connect to internet");
    }
  }

  buildSnackbar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: HexColor(topTextColor)),
      ),
      backgroundColor: HexColor(topBackColor),
    ));
  }

  _optionsDialogBox(BuildContext context) {
    return showDialog(
      //barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              /*  mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,*/

              children: <Widget>[
                AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  content: new SingleChildScrollView(
                    child: Container(
                      child: new ListBody(
                        children: <Widget>[
                          GestureDetector(
                            child: new Icon(
                              Icons.camera_alt,
                              size: 70.0,
                              color: HexColor(topBackColor),
                            ),
                            onTap: () => _openCamera(context),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          GestureDetector(
                              child: new Icon(
                                Icons.photo,
                                size: 70.0,
                                color: HexColor(topBackColor),
                              ),
                              onTap: () => _openGallary(context)),
                        ],
                      ),
                    ),
                  ),
                  actions: <Widget>[],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  _openCamera(BuildContext context) async {
    image = await ImagePicker.pickImage(
      source: ImageSource.camera,
    );
    setState(() {
      //this.image;
      _load = false;
      _isFromCamera = true;
    });

    _sendMEssageApi("", image);
    print(image);
    Navigator.pop(context);
    //print(picture);
  }

  _openGallary(BuildContext context) async {
    image = await ImagePicker.pickImage(
      source: ImageSource.gallery,
    );
    setState(() {
      _load = false;
      _isFromCamera = false;
    });

    _sendMEssageApi("", image);
    print(image);
    Navigator.pop(context);
  }
}
