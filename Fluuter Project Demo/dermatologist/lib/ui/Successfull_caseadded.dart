import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

import 'DashBoardScreen.dart';

class Successfull_caseadded extends StatefulWidget {
  UserModel userModel;
  UserData userData;

  Successfull_caseadded(this.userModel, this.userData);

  @override
  _MySuccessfull_caseaddedPages createState() =>
      _MySuccessfull_caseaddedPages();
}

class _MySuccessfull_caseaddedPages extends State<Successfull_caseadded> {



  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";

  @override
  void initState() {
    // TODO: implement initState
    setPrefrenceData();
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: HexColor(topBackColor),
        actions: <Widget>[
          Container(
            color: HexColor(topBackColor),
          )
        ],
        title: Text(
          "",
          style: TextStyle(color: HexColor(topTextColor)),
        ),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
//            new Container(
//              margin: EdgeInsets.fromLTRB(0.0, 10, 0.0, 0.0),
//              child: Center(
//                child: new SvgPicture.asset(
//                  "assets/images/check.svg",
//                  fit: BoxFit.fitWidth,
//                  height: 60,
//                  width: 60,
//                ),
//              ),
//            ),
            new Container(
              margin: EdgeInsets.fromLTRB(0.0, 10, 0.0, 0.0),
              child: Center(
                child: new Text("Thanks.",
                    style: TextStyle(color: HexColor(topBackColor), fontSize: 25),
                    textAlign: TextAlign.center),
              ),
            ),
            new Container(
              margin: EdgeInsets.fromLTRB(0.0, 5, 0.0, 0.0),
              child: Center(
                child: new Text("Your case has been added successfully!",
                    style: TextStyle(color: HexColor(topBackColor), fontSize: 18),
                    textAlign: TextAlign.center),
              ),
            ),
            new Container(
              margin: EdgeInsets.fromLTRB(0.0, 5, 0.0, 0.0),
              child: Center(
                child: new Text("Our dermatologist will get back to you soon.",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                    textAlign: TextAlign.center),
              ),
            ),
            new Container(
              margin: EdgeInsets.fromLTRB(30, 20, 30, 0.0),
              height: 40,
              width: double.infinity,
              child: ButtonTheme(
                child: RaisedButton(
                  shape: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(color: HexColor(topBackColor))),
                  child: Text(
                    "DONE",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    SystemChannels.textInput.invokeMethod('TextInput.hide');
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => new DashBoardScreen(
                          widget.userData.getUser(), widget.userData)),
                          (Route<dynamic> route) => false,
                    );

//                    Navigator.of(context).pushReplacement(new MaterialPageRoute(
//                        builder: (context) => new DashBoardScreen(
//                            widget.userData.getUser(), widget.userData)));
                  },
                  color: HexColor(topBackColor),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
