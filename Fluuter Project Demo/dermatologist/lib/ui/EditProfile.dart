import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:async/async.dart';
import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/ui/CustomInput.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';

class EditProfile extends StatefulWidget {
  UserModel userModel;
  UserData userData;

  EditProfile(this.userModel, this.userData);

  @override
  State<StatefulWidget> createState() => new _ProfilePageState();
}

class _ProfilePageState extends State<EditProfile> {
  File image;
  bool _isFromCamera = false;
  bool _isUpdateData = false;
  bool _load = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();

  DateTime selectedDate = DateTime.now();
  TextEditingController _date = new TextEditingController();

  String str_firstname, str_lastname, str_password;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";
  int _state = 0;

  @override
  void initState() {
    // TODO: implement initState
    setPrefrenceData();
    super.initState();
  }

  _optionsDialogBox(BuildContext context) {
    return showDialog(
      //barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              /*  mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,*/

              children: <Widget>[
                AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  content: new SingleChildScrollView(
                    child: Container(
                      child: new ListBody(
                        children: <Widget>[
                          GestureDetector(
                            child: new Icon(
                              Icons.camera_alt,
                              size: 70.0,
                              color: AppColors.blue,
                            ),
                            onTap: () => _openCamera(context),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          GestureDetector(
                              child: new Icon(
                                Icons.photo,
                                size: 70.0,
                                color: AppColors.blue,
                              ),
                              onTap: () => _openGallary(context)),
                        ],
                      ),
                    ),
                  ),
                  actions: <Widget>[],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

//  _openCamera(BuildContext context) async {
//    image = await ImagePicker.pickImage(
//      source: ImageSource.camera,
//    );
//    setState(() async {
//      //this.image;
//      _isFromCamera = true;
//    });
//
//    Navigator.pop(context);
//    //print(picture);
//  }

  _openCamera(BuildContext context) async {
    image = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
      _isFromCamera = true;
    });
    print(image);
    Navigator.pop(context);
  }

  _openGallary(BuildContext context) async {
    image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _isFromCamera = true;
    });
    print(image);
    Navigator.pop(context);
  }

//  _openGallary(BuildContext context) async {
//    image = await ImagePicker.pickImage(
//      source: ImageSource.gallery,
//    );
//    setState(() {
//      _isFromCamera = true;
//    });
//
//    print(image);
//    Navigator.pop(context);
//  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();
    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });

    _sharedPref.getFirstName().then((firstName) {
      firstNameController = TextEditingController(text: firstName);
    });

    _sharedPref.getLastName().then((lastName) {
      lastNameController = TextEditingController(text: lastName);
    });

    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = _load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: HexColor(topBackColor),
          title: new Text("My Profile",
              style: TextStyle(fontSize: 25.0, color: HexColor(topTextColor))),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: HexColor(topTextColor)),
            onPressed: () => Navigator.of(context).pop("Hi Helloooo"),
          ),
          elevation: 4.0,
        ),
        body: Center(
          child: ListView(
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => _optionsDialogBox(context),
                      child: new Container(
                        height: 160.0,
                        width: 160.0,
                        margin:
                            EdgeInsets.symmetric(vertical: 10, horizontal: 0.0),
                        child: new ClipRRect(
                          borderRadius: new BorderRadius.circular(8.0),
                          child: _isFromCamera
                              ? image == null
                                  ? new Stack(
                                      children: <Widget>[
                                        new Center(
                                          child: new CircleAvatar(
                                            radius: 100.0,
                                            backgroundColor:
                                                const Color(0xFFFFFFFF),
                                          ),
                                        ),
                                        new Center(
                                          child: new Image.asset(
                                            "assets/images/boy.png",
                                            height: 160.0,
                                            width: 160.0,
                                          ),
                                        ),
                                      ],
                                    )
                                  : new Container(
                                      height: 160.0,
                                      width: 160.0,
                                      decoration: new BoxDecoration(
                                        color: const Color(0xff7c94b6),
                                        image: new DecorationImage(
                                          image: new FileImage(
                                              new File(image.path)),
                                          fit: BoxFit.cover,
                                        ),
                                        border: Border.all(
                                            color: Colors.white, width: 5.0),
                                        borderRadius: new BorderRadius.all(
                                            const Radius.circular(80.0)),
                                      ),
                                    )
                              : widget.userModel.image == null ||
                                      widget.userModel.image.isEmpty
                                  ? image == null
                                      ? new Stack(
                                          children: <Widget>[
                                            new Center(
                                              child: new CircleAvatar(
                                                radius: 100.0,
                                                backgroundColor:
                                                    const Color(0xFFFFFFFF),
                                              ),
                                            ),
                                            new Center(
                                              child: new Image.asset(
                                                "assets/images/boy.png",
                                                width: 100,
                                                height: 100,
                                              ),
                                            ),
                                          ],
                                        )
                                      : new Container(
                                          height: 160.0,
                                          width: 160.0,
                                          decoration: new BoxDecoration(
                                            color: const Color(0xff7c94b6),
                                            image: new DecorationImage(
                                              image: new FileImage(
                                                  new File(image.path)),
                                              fit: BoxFit.cover,
                                            ),
                                            border: Border.all(
                                                color: Colors.white,
                                                width: 5.0),
                                            borderRadius: new BorderRadius.all(
                                                const Radius.circular(80.0)),
                                          ),
                                        )
                                  : new Container(
                                      height: 160.0,
                                      width: 160.0,
                                      decoration: new BoxDecoration(
                                        color: const Color(0xff7c94b6),
                                        image: new DecorationImage(
                                          image: new NetworkImage(
                                              widget.userModel.image),
                                          fit: BoxFit.cover,
                                        ),
                                        border: Border.all(
                                            color: Colors.white, width: 5.0),
                                        borderRadius: new BorderRadius.all(
                                            const Radius.circular(80.0)),
                                      ),
                                    ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30),
                child: Container(
                  child: CustomInput(
                    obscureText: false,
                    controllertext: firstNameController,
                    inputIcon: Icons.person_outline,
                    enabled: true,
                    keyboardType: TextInputType.text,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30),
                child: Container(
                  child: CustomInput(
                      obscureText: false,
                      controllertext: lastNameController,
                      inputIcon: Icons.person_outline,
                      enabled: true,
                      keyboardType: TextInputType.text),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30),
                child: Container(
                  child: CustomInput(
                      obscureText: false,
                      controllertext: emailController,
                      inputText: widget.userModel.email,
                      inputIcon: Icons.email,
                      enabled: false,
                      keyboardType: TextInputType.emailAddress),
                ),
              ),
//              Padding(
//                padding: const EdgeInsets.only(left: 30.0, right: 30.0),
//                child: Container(
//                  child: GestureDetector(
//                    onTap: () => _selectDate(context),
//                    child: AbsorbPointer(
//                      child: TextFormField(
//                        controller: _date,
//                        keyboardType: TextInputType.datetime,
//                        decoration: InputDecoration(
//                          hintText: 'Date of Birth',
//                          prefixIcon: Icon(
//                            Icons.dialpad,
//                            color: Colors.black,
//                          ),
//                        ),
//                      ),
//                    ),
//                  ),
//                ),
//              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30),
                child: Container(
                  child: CustomInput(
                    obscureText: true,
                    controllertext: passwordController,
                    inputText: "Password",
                    inputIcon: Icons.lock_outline,
                    enabled: true,
                    keyboardType: TextInputType.emailAddress,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 30.0, right: 30),
                child: Container(
                  child: CustomInput(
                    obscureText: true,
                    controllertext: confirmPasswordController,
                    inputText: "Confirm Password",
                    inputIcon: Icons.lock_outline,
                    enabled: true,
                    keyboardType: TextInputType.emailAddress,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: new Container(
                  height: 60,
                  width: double.infinity,
                  margin: new EdgeInsets.symmetric(horizontal: 30.0),
                  child: ButtonTheme(
                    height: 60,
                    child: RaisedButton(
                      shape: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              BorderSide(color: HexColor(topBackColor))),
                      child: setUpButtonChild(),
                      onPressed: () {
                        checkInternetConnection(context);
                      },
                      color: HexColor(topBackColor),
                    ),
                  ),
                ),
              ),
            ],
            /* child:
          ),*/
          ),
        ));
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1901, 1),
        lastDate: DateTime(2100));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _date.value = TextEditingValue(text: picked.toString());
      });
  }

  checkInternetConnection(BuildContext context) async {
    if (await HttpHelper.isConnectedNew(context, checkInternetConnection,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, checkInternetConnection,
            barrierDismissible: true)) {
      _updateData(context);
    } else {
      buildSnackbar("Please connect to internet");
    }
  }

  buildSnackbar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: HexColor(topTextColor)),
      ),
      backgroundColor: HexColor(topBackColor),
    ));
  }

  _updateData(BuildContext context) {
    String firstname, lastname;
    firstname = firstNameController.text;
    lastname = lastNameController.text;
//    if (firstNameController.text == "") {
//
//    } else {
//      firstname = firstNameController.text;
//    }
//    String lastname;
//    if (lastNameController.text == "") {
//
//    } else {
//      lastname = lastNameController.text;
//    }
    String password = passwordController.text;

    String confirm_password = confirmPasswordController.text;

    if (firstname.length == 0) {
      buildSnackbar("Please Enter First Name");
    } else if (lastname.length == 0) {
      buildSnackbar("Please Enter Last Name");
    } else {
      if (password.length != 0) {
        if (password.length <= 4) {
          buildSnackbar("Password must be more then 4 Character ");
        } else if (password != confirm_password) {
          buildSnackbar("Password and confirm password does not match");
        } else {
          setState(() {
            _load = true;
            if (_state == 0) {
              animateButton();
            }
          });
          UpdateProfileData(firstname, lastname, password,
              widget.userModel.user_id, image, context);
        }
      } else {
        setState(() {
          _load = true;
          if (_state == 0) {
            animateButton();
          }
        });
        UpdateProfileData(firstname, lastname, password,
            widget.userModel.user_id, image, context);
      }
    }
  }

  UpdateProfileData(String firstname, String lastName, String password,
      String userId, File image, BuildContext context) async {
    var uri = Uri.parse(HttpHelper.url + HttpHelper.updateProfileAPI);

    var request = new http.MultipartRequest("POST", uri);
    var data = {
      'user_id': userId,
      'firstname': firstname,
      'lastname': lastName,
      'password': password,
      'admin_id': HttpHelper.admin_id,
    };

    request.fields.addAll(data);

    if (image != null) {
      var stream =
          new http.ByteStream(DelegatingStream.typed(image.openRead()));
      var length = await image.length();

      var multipartFile = new http.MultipartFile('image', stream, length,
          filename: basename(image.path));
      request.files.add(multipartFile);
    }
    var response = await request.send();
    print(response.toString());
    response.stream.transform(utf8.decoder).listen((value) {
      var data = json.decode(value);
      print(data['id']);
      if (data['status']) {
        UserModel user = UserModel.fromJson(data['user']);
        setState(() {
          widget.userData.setUser(user);
          _load = false;
          _state = 0;
        });
        buildSnackbar(data['message']);
        Timer _timer = new Timer(const Duration(milliseconds: 2000), () {
          setState(() {
            Navigator.of(context).pop("Hi");
          });
        });
      } else {
        setState(() {
          _load = false;
          _state = 0;
        });
        buildSnackbar(data['message']);
      }

      return Future<String>.value(value);
    }).onError((error) {
      setState(() {
        _load = false;
        _state = 0;
      });
      print("Error: $error");
      return Future<String>.value("Error $error");
    });
  }

  Widget setUpButtonChild() {
    if (_state == 0) {
      return new Text(
        "Update Profile",
        style: const TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      );
    } else if (_state == 1) {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    }
  }

  void animateButton() {
    setState(() {
      _state = 1;
    });

//    Timer(Duration(milliseconds: 3300), () {
//      setState(() {
//        _state = 2;
//      });
//    });
  }
}
