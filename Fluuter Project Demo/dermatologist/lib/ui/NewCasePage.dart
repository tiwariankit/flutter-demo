import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:io'
    show
        File,
        HttpClient,
        HttpClientResponse,
        InternetAddress,
        Platform,
        SocketException;
import 'dart:math';

import 'package:async/async.dart';
import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/payment/PaymentModel.dart';
import 'package:dermatologist/payment/PaypalPayment.dart';
import 'package:dermatologist/util/DBProvider.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqlite_api.dart';

import 'DashBoardScreen.dart';
import 'Successfull_caseadded.dart';

class NewCasePage extends StatefulWidget {
  bool isAppBar = false;
  UserModel userModel;
  UserData userData;

  NewCasePage(this.isAppBar, this.userModel, this.userData);

  @override
  _MyNewCasePage createState() => _MyNewCasePage();
}

class _MyNewCasePage extends State<NewCasePage> {
  File _image, imageSecond;

  var _isFromCamera = false, _isFromCameraSecond = false;
  bool isDataSubmit = false;
  bool isDays = true, isWeeks = false, isMonth = false, isYears = false;
  TextEditingController _textFieldController_name = TextEditingController();
  TextEditingController _textFieldController_contactNumber =
      TextEditingController();
  TextEditingController _textFieldController_description =
      TextEditingController();

  int _currValue = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _load = false;
  List<File> imageList = new List();
  ScrollController _scrollController = new ScrollController();

  bool isFastSelect = true, isMediumSelect = false, isLowSelect = false;
  var buttonPrice = "";
  var transction_id = "", payment_status = "";
  String name = "";
  String contact_number = "";
  String description = "";
  String conditions = "";
  String sex = "";
  String sandboxKey = "", sandbox_companyName = "";
  PersistentBottomSheetController _controller; // <------ Instance variable
  BuildContext mcontext;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF",
      app_id = "",
      user_id = "",
      fullName = "";

  @override
  void initState() {
    // TODO: implement initState
    setPrefrenceData();
//    _paypalKey();
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();

    SharedPref.getAdminId().then((id) {
      setState(() {
        app_id = id;
      });
    });

    _sharedPref.getUserId().then((id) {
      setState(() {
        user_id = id;
      });
    });

    _sharedPref.getFullName().then((name) {
      setState(() {
        _textFieldController_name = TextEditingController(text: name);
      });
    });

    _sharedPref.getTokenizationKey().then((key) {
      setState(() {
        sandboxKey = key;
      });
    });

    _sharedPref.getCompanyName().then((name) {
      setState(() {
        sandbox_companyName = name;
      });
    });

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {


    var width            = MediaQuery.of(context).size.width;
    var btnDurationwidth = width / 4 - (30);

    final FocusNode name        = FocusNode();
    final FocusNode contactNo   = FocusNode();
    final FocusNode description = FocusNode();


    this.mcontext = context;
    Widget loadingIndicator = _load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

    return Scaffold(
      key: _scaffoldKey,
      appBar: widget.isAppBar
          ? AppBar(
              backgroundColor: HexColor(topBackColor),
              leading: new IconButton(
                icon: new Icon(Icons.arrow_back, color: HexColor(topTextColor)),
                onPressed: () => Navigator.of(context).pop("Hi Helloooo"),
              ),
              actions: <Widget>[],
              iconTheme: IconThemeData(
                color: HexColor(topBackColor),
              ),
              title: Text(
                "New case",
                style: TextStyle(color: HexColor(topTextColor)),
              ),
            )
          : AppBar(
              backgroundColor: Colors.white,
              elevation: 0.0,
              toolbarOpacity: 0.0,
            ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          child: SingleChildScrollView(
            controller: _scrollController,
            child: AbsorbPointer(
              ignoringSemantics: _load ? true : false,
              absorbing: _load ? true : false,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 5,
                          child: new GestureDetector(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: Container(
                                height: 150,
                                color: Color(0xFFEEEEEE),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    _isFromCamera
                                        ? _image == null || _image.path == null
                                            ? Icon(
                                                Icons.camera_alt,
                                                size: 70,
                                                color: Color(0xFFBDBDBD),
                                              )
                                            : new Container(
                                                height: 130.0,
                                                width: double.infinity,
                                                decoration: new BoxDecoration(
                                                  color: const Color(0xff7c94b6),
                                                  image: new DecorationImage(
                                                    image: new FileImage(
                                                        new File(_image.path)),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ))
                                        : Icon(
                                            Icons.camera_alt,
                                            size: 70,
                                            color: Color(0xFFBDBDBD),
                                          ),
                                    Text(
                                      "Upload Photo 1",
                                      style: TextStyle(color: Color(0xFFBDBDBD)),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            onTap: () => _optionsDialogBox(context, true),
                          ),
                        ),
                        Expanded(
                          flex: 5,
                          child: new GestureDetector(
                            child: Padding(
                              padding: const EdgeInsets.only(right: 5),
                              child: Container(
                                height: 150,
                                color: Color(0xFFEEEEEE),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    _isFromCameraSecond
                                        ? imageSecond == null ||
                                                imageSecond.path == null
                                            ? Icon(
                                                Icons.camera_alt,
                                                size: 70,
                                                color: Color(0xFFBDBDBD),
                                              )
                                            : new Container(
                                                height: 130.0,
                                                width: double.infinity,
                                                decoration: new BoxDecoration(
                                                  color: const Color(0xff7c94b6),
                                                  image: new DecorationImage(
                                                    image: new FileImage(new File(
                                                        imageSecond.path)),
                                                    fit: BoxFit.cover,
                                                  ),
                                                ))
                                        : Icon(
                                            Icons.camera_alt,
                                            size: 70,
                                            color: Color(0xFFBDBDBD),
                                          ),
                                    Text(
                                      "Upload Photo 2",
                                      style: TextStyle(color: Color(0xFFBDBDBD)),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            onTap: () => _optionsDialogBox(context, false),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    "How long have you had this condition?",
                    style: TextStyle(fontSize: 15, color: Colors.black),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Scrollbar(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,

                        physics: ScrollPhysics(),
                        child: Row(
                          children: <Widget>[
                            MaterialButton(
                              height: 40,
                              minWidth: btnDurationwidth,
                              color: isDays ? AppColors.blue : Color(0xFFE0E0E0),
                              child: Text(
                                "Days",
                                style: TextStyle(
                                    color:
                                        isDays ? Colors.white : Color(0xFF757575),
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                              onPressed: () {
                                conditionsCheck(0);
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 22),
                              child: MaterialButton(
                                color: isWeeks ? AppColors.blue : Color(0xFFE0E0E0),
                                height: 40,
                                minWidth: btnDurationwidth,
                                child: Text(
                                  "Weeks",
                                  style: TextStyle(
                                      color: isWeeks
                                          ? Colors.white
                                          : Color(0xFF757575),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                onPressed: () {
                                  conditionsCheck(1);
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 22),
                              child: MaterialButton(
                                color: isMonth ? AppColors.blue : Color(0xFFE0E0E0),
                                height: 40,
                                minWidth: btnDurationwidth,
                                child: Text(
                                  "Months",
                                  style: TextStyle(
                                      color: isMonth
                                          ? Colors.white
                                          : Color(0xFF757575),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                onPressed: () {
                                  conditionsCheck(2);
                                },
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 22),
                              child: MaterialButton(
                                color: isYears ? AppColors.blue : Color(0xFFE0E0E0),
                                height: 40,
                                minWidth: btnDurationwidth,
                                child: Text(
                                  "Years",
                                  style: TextStyle(
                                      color: isYears
                                          ? Colors.white
                                          : Color(0xFF757575),
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                onPressed: () {
                                  conditionsCheck(3);
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Stack(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15)),
                          child: Container(
                            width: double.infinity,
                            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10),
                            child: Padding(
                              padding: const EdgeInsets.only(left: 15, right: 15),
                              child: Column(
                                children: <Widget>[
                                  TextField(
                                      onChanged: (text) {
                                        print(text);
                                      },
                                      textInputAction: TextInputAction.next,
                                      autofocus: false,
                                      focusNode: name,
                                      onEditingComplete: () => FocusScope.of(context).requestFocus(contactNo),
                                      controller: _textFieldController_name,
                                      decoration: InputDecoration(
                                          labelText: "Name",
                                          labelStyle: TextStyle(fontSize: 18))),
                                  TextField(
                                      focusNode: contactNo,
                                      keyboardType: TextInputType.number,
                                      maxLength: 10,
                                      controller:
                                          _textFieldController_contactNumber,
                                      onChanged: (text) {
                                        print(text);
                                      },
                                      textInputAction: TextInputAction.next,
                                      autofocus: false,
                                      decoration: InputDecoration(
                                          labelText: "Contact No.",
                                          labelStyle: TextStyle(fontSize: 18))),
                                  /*Container(
                              height: 250,
                              child: TextField(

                                  decoration: InputDecoration(labelText: "Sex."),
                              ),
                            )*/
                                  Padding(
                                    padding: const EdgeInsets.only(top: 15),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "Sex",
                                          style: TextStyle(
                                              color: AppColors.grey,
                                              fontSize: 18),
                                        ),
                                        Row(
                                          children: <Widget>[
                                            new Radio(
                                              value: 0,
                                              groupValue: _currValue,
                                              onChanged: (int i) =>
                                                  setState(() => _currValue = i),
                                            ),
                                            new Text(
                                              'Male',
                                              style:
                                                  new TextStyle(fontSize: 16.0),
                                            ),
                                            new Radio(
                                              value: 1,
                                              groupValue: _currValue,
                                              onChanged: (int i) =>
                                                  setState(() => _currValue = i),
                                            ),
                                            new Text(
                                              'Female',
                                              style: new TextStyle(
                                                fontSize: 16.0,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: 1,
                                    width: double.infinity,
                                    color: Color(0xFF616161),
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(top: 15),
                                        child: TextField(
                                            onChanged: (text) {
                                              print(text);
                                            },
                                            controller:
                                                _textFieldController_description,
                                            keyboardType: TextInputType.text,
                                            maxLines: null,
                                            textInputAction: TextInputAction.done,
                                            decoration: InputDecoration(
                                                labelText: "Description",
                                                labelStyle:
                                                    TextStyle(fontSize: 18))),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Align(
                        child: loadingIndicator,
                        alignment: FractionalOffset.center,
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: 20, left: 10, right: 10),
                    child: Container(
                      height: 50,
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(vertical: 0.0, horizontal: 20),
                      child: RaisedButton(
                        color: AppColors.blue,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)),
                        child: Text(
                          "Continue",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
//                      var rndnumber = "";
//                      var rnd = new Random();
//                      for (var i = 0; i < 6; i++) {
//                        rndnumber = rndnumber + rnd.nextInt(9).toString();
//                      }
//                      print(rndnumber);
                          FocusScope.of(context).requestFocus(new FocusNode());
                          submitData(context);
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  double screenHeight(BuildContext context, {double dividedBy = 2.5}) {
    return screenSize(context).height / dividedBy;
  }

  Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  openBottomSheetDialog(BuildContext context) {
    buttonPrice = "59.95";
    isFastSelect = true;
    isMediumSelect = false;
    isLowSelect = false;
    showModalBottomSheet(
        context: context,

        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setModelState) {
            return Container(
              height: screenHeight(context),
              child: new Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: new Text(
                      "CHOOSE RESPONSE TIME",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: Colors.black),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: <Widget>[
                          InkWell(
                            onTap: () =>
                                {checkStateForPackage(0, setModelState)},
                            child: new Container(
                              padding: EdgeInsets.all(10.0),
                              margin: EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  color: isFastSelect
                                      ? HexColor(topBackColor)
                                      : Colors.white),
//                              height: 120,
                              width: 100,
                              child: Column(
                                children: <Widget>[
                                  new ClipRRect(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    child: Column(
                                      children: <Widget>[
                                        new SvgPicture.asset(
                                          "assets/images/fast.svg",
                                          fit: BoxFit.fitWidth,
                                          height: 40,
                                          width: 40,
                                          color: isFastSelect
                                              ? Colors.white
                                              : HexColor(topBackColor),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 10, 0, 0),
                                          child: new Text(
                                            "8",
                                            style: TextStyle(
                                                color: isFastSelect
                                                    ? Colors.white
                                                    : HexColor(topBackColor),
                                                fontWeight: FontWeight.w600,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 10, 0, 0),
                                          child: new Text(
                                            "\$59.95",
                                            style: TextStyle(
                                                color: isFastSelect
                                                    ? Colors.white
                                                    : HexColor(topBackColor),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 15),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () =>
                                {checkStateForPackage(1, setModelState)},
                            child: new Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 10),
                              padding: EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  color: isMediumSelect
                                      ? HexColor(topBackColor)
                                      : Colors.white),
//                              height: 120,
                              width: 100,
                              child: Column(
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      new SvgPicture.asset(
                                        "assets/images/medium.svg",
                                        fit: BoxFit.fitWidth,
                                        height: 40,
                                        width: 40,
                                        color: isMediumSelect
                                            ? Colors.white
                                            : HexColor(topBackColor),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0.0, 10, 0, 0),
                                        child: new Text(
                                          "24",
                                          style: TextStyle(
                                              color: isMediumSelect
                                                  ? Colors.white
                                                  : HexColor(topBackColor),
                                              fontWeight: FontWeight.w600,
                                              fontSize: 18),
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0.0, 10, 0, 0),
                                        child: new Text(
                                          "\$39.95",
                                          style: TextStyle(
                                              color: isMediumSelect
                                                  ? Colors.white
                                                  : HexColor(topBackColor),
                                              fontWeight: FontWeight.w500,
                                              fontSize: 15),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          InkWell(
                            onTap: () =>
                                {checkStateForPackage(2, setModelState)},
                            child: new Container(
                              padding: EdgeInsets.all(10.0),
                              margin: EdgeInsets.symmetric(
                                  vertical: 0.0, horizontal: 10),
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  color: isLowSelect
                                      ? HexColor(topBackColor)
                                      : Colors.white),
//                              height: 120,
                              width: 100,
                              child: Column(
                                children: <Widget>[
                                  new ClipRRect(
                                    borderRadius:
                                        new BorderRadius.circular(5.0),
                                    child: Column(
                                      children: <Widget>[
                                        new SvgPicture.asset(
                                          "assets/images/low.svg",
                                          fit: BoxFit.fitWidth,
                                          height: 40,
                                          width: 40,
                                          color: isLowSelect
                                              ? Colors.white
                                              : HexColor(topBackColor),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 10, 0, 0),
                                          child: new Text(
                                            "48",
                                            style: TextStyle(
                                                color: isLowSelect
                                                    ? Colors.white
                                                    : HexColor(topBackColor),
                                                fontWeight: FontWeight.w600,
                                                fontSize: 18),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(
                                              0.0, 10, 0, 0),
                                          child: new Text(
                                            "\$29.95",
                                            style: TextStyle(
                                                color: isLowSelect
                                                    ? Colors.white
                                                    : HexColor(topBackColor),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 15),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: 10, left: 10, right: 10),
                    child: Container(
                      height: 50,
                      width: double.infinity,
                      margin:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                      child: RaisedButton(
                        color: AppColors.blue,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0)),
                        child: Text(
                          "PAY \$${buttonPrice} AND SUBMIT CASE",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                          _payment(context);
//                          redirectScreen(context);
                        },
                      ),
                    ),
                  )
                ],
              ),
            );
          });
        });
  }

  submitData(BuildContext context) async {
    imageList = new List();

    if (_image != null && _image.path != null && _isFromCamera) {
      imageList.add(_image);
    }
    if (imageSecond != null &&
        imageSecond.path != null &&
        _isFromCameraSecond) {
      imageList.add(imageSecond);
    }
//
    name = _textFieldController_name.text;
    contact_number = _textFieldController_contactNumber.text;
    description = _textFieldController_description.text;
    conditions = "";
    sex = "";
    if (isDays) {
      conditions = "1";
    } else if (isWeeks) {
      conditions = "2";
    } else if (isMonth) {
      conditions = "3";
    } else if (isYears) {
      conditions = "4";
    }

    if (_currValue == 0) {
      sex = "1";
    } else if (_currValue == 1) {
      sex = "2";
    }

    if (imageList.length == 0) {
      buildSnackbar("Please Upload Photo");
      _scrollController.jumpTo(0);
//      _scrollController.animateTo(0, duration: Duration(days: 0,hours: 0,minutes: 0,seconds: 5,microseconds: 0), curve: null);
    } else if (imageList.length != 2) {
      buildSnackbar("Please Upload both Photo");
      _scrollController.jumpTo(0);
    } else if (name.length == 0) {
      buildSnackbar("Please Enter Name");
    } else if (contact_number.length == 0) {
      buildSnackbar("Please Enter Mobile Number");
    } else if (contact_number.length < 10) {
      buildSnackbar("Please Enter Valid Mobile Number");
    } else if (description.length == 0) {
      buildSnackbar("Please Enter Decription");
    } else {
      _checkuserIsOnline(context);
    }
  }

  _checkuserIsOnline(context) async {
    if (await HttpHelper.isConnectedNew(context, _checkuserIsOnline,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, _checkuserIsOnline,
            barrierDismissible: true)) {
      openBottomSheetDialog(context);
    } else {
      _checkIfDataisExist(context);
    }
  }

  _checkIfDataisExist(BuildContext context) async {
    Database db = await DBProvider.instance.database;

    var rndnumber = "";
    var rnd = new Random();
    for (var i = 0; i < 6; i++) {
      rndnumber = rndnumber + rnd.nextInt(9).toString();
    }
    List<String> filePath = new List();
    for (int j = 0; j < imageList.length; j++) {
      filePath.add(imageList[j].path);
    }

    DateTime now = DateTime.now();
    String date = DateFormat('MM-dd-yyyy').format(now);
    String time = DateFormat('KK:mm a').format(now);
//    String formattedDate = DateFormat('KK:MM:ss a \n yyyy/MM/dd').format(now);
    print(time);
    print(date);
    Map<String, dynamic> row = {
      DBProvider.admin_id: app_id,
      DBProvider.user_id: user_id,
      DBProvider.id: "",
      DBProvider.case_id: rndnumber,
      DBProvider.photos: filePath.toString(),
      DBProvider.problem_occur: conditions,
      DBProvider.Name: name,
      DBProvider.contact: contact_number,
      DBProvider.sex: sex,
      DBProvider.description: description,
      DBProvider.transaction_id: "",
      DBProvider.amount: "",
      DBProvider.status: "1",
      DBProvider.date: date,
      DBProvider.time: time,
      DBProvider.is_offline: "1"
    };
    await db.insert(DBProvider.case_list, row);

    SystemChannels.textInput.invokeMethod('TextInput.hide');
    buildSnackbar("Successfully draft saved");

    new Future.delayed(const Duration(seconds: 2), () {
      Navigator.of(context).pushReplacement(new MaterialPageRoute(
          builder: (context) =>
              new DashBoardScreen(widget.userData.getUser(), widget.userData)));
    });
  }

  sendMultipleImage(
      BuildContext context,
      String conditions,
      String name,
      String contact_number,
      String sex,
      String description,
      File image,
      String id,
      String status,
      String transaction_id) async {
    if (await HttpHelper.isConnectedNew(context, sendMultipleImage,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, sendMultipleImage,
            barrierDismissible: true)) {
      var uri = Uri.parse(HttpHelper.cases + HttpHelper.addCases);
      print(HttpHelper.cases + HttpHelper.addCases);
      var request = new http.MultipartRequest("POST", uri);
      var data = {
        'condition': conditions,
        'name': name,
        'contact': contact_number,
        'sex': sex,
        'description': description,
        'user_id': widget.userModel.user_id,
        'id': id,
        'status': status,
        'transaction_id': transaction_id,
        'admin_id': HttpHelper.admin_id,
      };
      print(data);
      request.fields.addAll(data);

      if (image != null) {
        var stream =
            new http.ByteStream(DelegatingStream.typed(image.openRead()));
        var length = await image.length();

        var multipartFile = new http.MultipartFile('photo', stream, length,
            filename: basename(image.path));
        request.files.add(multipartFile);
      }
      var response = await request.send();
      print(response.toString());
      response.stream.transform(utf8.decoder).listen((value) {
        var data = json.decode(value);
        print(data['id']);
        if (data['status']) {
          if (imageList.length > 1) {
            imageList.removeAt(0);
            sendMultipleImage(
                context,
                conditions,
                name,
                contact_number,
                sex,
                description,
                imageList[0],
                data['id'].toString(),
                status,
                transaction_id);
          } else {
            buildSnackbar(data['message']);
            setState(() {
              _load = false;
              clearAllData();
            });

            redirectScreen();
          }
        } else {
          setState(() {
            _load = false;
          });
          buildSnackbar(data['message']);
        }

        return Future<String>.value(value);
      }).onError((error) {
        setState(() {
          _load = false;
        });
        print("Error: $error");
        return Future<String>.value("Error $error");
      });
    }
  }

  buildSnackbar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: HexColor(topTextColor)),
      ),
      backgroundColor: HexColor(topBackColor),
    ));
  }

  void redirectScreen() {
    new Future.delayed(const Duration(seconds: 2), () {
      Navigator.of(mcontext).pushReplacement(new MaterialPageRoute(
          builder: (context) => new Successfull_caseadded(
              widget.userData.getUser(), widget.userData)));
    });
  }

  void conditionsCheck(int index) {
    switch (index) {
      case 0:
        setState(() {
          isDays = true;
          isWeeks = false;
          isMonth = false;
          isYears = false;
        });
        break;
      case 1:
        setState(() {
          isDays = false;
          isWeeks = true;
          isMonth = false;
          isYears = false;
        });
        break;
      case 2:
        setState(() {
          isDays = false;
          isWeeks = false;
          isMonth = true;
          isYears = false;
        });
        break;
      case 3:
        setState(() {
          isDays = false;
          isWeeks = false;
          isMonth = false;
          isYears = true;
        });
        break;
    }
  }

  void checkStateForPackage(int index, StateSetter stateSetter) {
    switch (index) {
      case 0:
        stateSetter(() {
          isFastSelect = true;
          isMediumSelect = false;
          isLowSelect = false;
          buttonPrice = "59.95";
        });
        break;
      case 1:
        stateSetter(() {
          isFastSelect = false;
          isMediumSelect = true;
          isLowSelect = false;
          buttonPrice = "39.95";
        });
        break;
      case 2:
        stateSetter(() {
          isFastSelect = false;
          isMediumSelect = false;
          isLowSelect = true;
          buttonPrice = "29.95";
        });
        break;
    }
  }

  _optionsDialogBox(BuildContext context, bool isFromFirst) {
    return showDialog(
      //barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        return Container(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              /*  mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,*/

              children: <Widget>[
                AlertDialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  content: new SingleChildScrollView(
                    child: Container(
                      child: new ListBody(
                        children: <Widget>[
                          GestureDetector(
                            child: new Icon(
                              Icons.camera_alt,
                              size: 70.0,
                              color: AppColors.blue,
                            ),
                            onTap: () => _openCamera(context, isFromFirst),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                          ),
                          GestureDetector(
                              child: new Icon(
                                Icons.photo,
                                size: 70.0,
                                color: AppColors.blue,
                              ),
                              onTap: () => _openGallary(context, isFromFirst)),
                        ],
                      ),
                    ),
                  ),
                  actions: <Widget>[],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  _openCamera(BuildContext context, bool isFromfirst) async {
    if (isFromfirst) {
      _image = await ImagePicker.pickImage(source: ImageSource.camera);
      setState(() {
        _isFromCamera = true;
      });

      GallerySaver.saveImage(_image.path);
    } else {
      imageSecond = await ImagePicker.pickImage(
        source: ImageSource.camera,
      );
      setState(() {
        _isFromCameraSecond = true;
      });

      GallerySaver.saveImage(imageSecond.path);
      print(imageSecond);
    }

    Navigator.pop(context);
    //print(picture);
  }

  _openGallary(BuildContext context, bool isFromfirst) async {
    if (isFromfirst) {
      _image = await ImagePicker.pickImage(source: ImageSource.gallery);
      setState(() {
        _isFromCamera = true;
      });
      print(_image);
    } else {
      imageSecond = await ImagePicker.pickImage(
        source: ImageSource.gallery,
      );
      setState(() {
        _isFromCameraSecond = true;
      });
      print(imageSecond);
    }

    Navigator.pop(context);
  }

  void clearAllData() {
    _isFromCamera = false;
    _isFromCameraSecond = false;
    // _image = null;
    imageSecond = null;
    _textFieldController_name.text = "";
    _textFieldController_contactNumber.text = "";
    _textFieldController_description.text = "";
    isDays = true;
    isWeeks = false;
    isWeeks = false;
    isMonth = false;
    isYears = false;
    _currValue = 0;
  }

  _payment(BuildContext context) async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PaypalPayment(
          buttonPrice,
          onFinish: (paymentModel) {
            PaymentModel model = paymentModel;
            print("PAYMENT DATA $paymentModel");
            print("ID ${model.id}");
            print("STATUS ${model.status}");
            print("FULL RESPONSE ${model.response}");
            _callPaymentApi(model, context);
          },
        ),
      ),
    );
  }

  _callPaymentApi(PaymentModel model, BuildContext context) async {
    var result = model.id;
    if (result != null) {
//      FocusScope.of(context).unfocus();
//      if (HttpHelper.isConnectedNew(context, _payment,
//                  barrierDismissible: true) !=
//              null &&
//          HttpHelper.isConnectedNew(context, _payment,
//              barrierDismissible: true)) {
      setState(() {
        _load = true;
      });
      String paymentTye = "";
      HttpHelper.checkoutAPI(buttonPrice, result, widget.userModel.user_id,
              paymentTye, model.response)
          .then((response) {
//        FocusScope.of(context).unfocus();
        print(response.body);
        var jsonParse = json.decode(response.body);
        if (jsonParse['status'])
        {
          bool status = jsonParse['status'];
          if (status) {
            payment_status = "1";
          } else {
            payment_status = "0";
          }

          transction_id = jsonParse['transation_id'];
          sendMultipleImage(context, conditions, name, contact_number, sex,
              description, imageList[0], "", payment_status, transction_id);
        } else {
          setState(() {
            _load = false;
          });
          buildSnackbar(jsonParse['message']);
        }
      }).catchError((error) {
        print(error);
        setState(() {
          _load = false;
        });
      });
//      }
    }
  }
}
