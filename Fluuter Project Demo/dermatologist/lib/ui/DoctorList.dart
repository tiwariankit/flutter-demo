import 'dart:convert' as JSON;

import 'package:dermatologist/model/DoctorListData.dart';
import 'package:dermatologist/ui/DoctorDetails.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';

class DoctorList extends StatefulWidget {
  DoctorList({Key key}) : super(key: key);

  @override
  _MyDoctorList createState() => _MyDoctorList();
}

class _MyDoctorList extends State<DoctorList> {
  int _currentIndex = 0;
  bool _load = false;

  //var list=new List<DoctorListData>();
  List<DoctorListData> list = new List();

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";


  @override
  void initState() {
    // TODO: implement initState
    setPrefrenceData();
    doctorList();
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });

  }


  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = !_load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: HexColor(topBackColor),
        actions: <Widget>[
          Container(
            color: HexColor(topBackColor),
          )
        ],
        leading: new IconButton(
            icon: new Icon(Icons.chevron_left, color: HexColor(topTextColor)),
            onPressed: () => Navigator.of(context).pop("Hi Helloooo")),
        title: Text(
          "Featured Doctor",
          style: TextStyle(color: HexColor(topTextColor)),
        ),
      ),
      body: _load
          ? ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
              scrollDirection: Axis.vertical,
              itemCount: list.length,
              itemBuilder: (context, i) {
                // If you've reached at the end of the available word pairs...
                return _buildRow(list[i], i);
              },
            )
          : Align(
              child: loadingIndicator,
              alignment: FractionalOffset.center,
            ),
    );
  }

  Widget _buildRow(DoctorListData data, int index) {
    return new ListTile(
      onTap: () {
        _doctorDetails(data.id);
      },
      title: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                new Container(
                    width: 70.0,
                    height: 70.0,
                    decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        image: new DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(
                                "http://dermatologist.fmv.cc/uploads/admins/" +
                                    data.image)))),
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Column(
                    children: <Widget>[
                      Text(
                        data.firstname,
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: Text(data.lastname),
                      )
                    ],
                  ),
                ),
                Spacer(),
                Icon(
                  Icons.chevron_right,
                  color: AppColors.grey,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  doctorList() {
    HttpHelper.doctorList().then((response) {
      print(response.body);
      var l =
          JSON.jsonDecode(response.body)['data'].cast<Map<String, dynamic>>();
      List<DoctorListData> list = l
          .map<DoctorListData>((json) => DoctorListData.fromJson(json))
          .toList();

      setState(() {
        this.list = list;
        _load = true;
      });
    }).catchError((error) {});
  }

  void _doctorDetails(String id) {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new DoctorDetails(id)));
  }
}
