import 'dart:convert' as JSON;

import 'package:dermatologist/model/DoctorDetailsData.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';

class DoctorDetails extends StatefulWidget {
  String id;

  DoctorDetails(this.id);

  // DoctorDetails({Key key}) : super(key: key);

  @override
  _MyDoctorDetails createState() => _MyDoctorDetails();
}

class _MyDoctorDetails extends State<DoctorDetails> {
  int _currentIndex = 0;
  List<DoctorDetailsData> list = new List();

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";


  @override
  void initState() {
    // TODO: implement initState
    HttpHelper.getAdminId();
    setPrefrenceData();
    doctorDetails();
    super.initState();
  }


  void setPrefrenceData() {
    _sharedPref = new SharedPref();

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });

  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: HexColor(topBackColor),
        actions: <Widget>[
          Container(
            color: HexColor(topBackColor),
          )
        ],
        leading: new IconButton(
            icon: new Icon(Icons.chevron_left, color: HexColor(topTextColor)),
            onPressed: () => Navigator.of(context).pop("Hi Helloooo")),
        title: Text(
          "Featured Doctor",
          style: TextStyle(color: HexColor(topTextColor)),
        ),
      ),
      body: ListView.separated(
        separatorBuilder: (context, index) => Divider(
          color: Colors.black,
        ),
        scrollDirection: Axis.vertical,
        itemCount: list.length,
        itemBuilder: (context, i) {
          // If you've reached at the end of the available word pairs...
          return _buildRow(list[i], i);
        },
      ),
    );
  }

  Widget _buildRow(DoctorDetailsData data, int index) {
    return new ListTile(
      title: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: new Container(
                      width: 70.0,
                      height: 70.0,
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: NetworkImage(
                                  "http://dermatologist.fmv.cc/uploads/admins/" +
                                      data.image)))),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 5),
                  child: Column(
                    children: <Widget>[
                      Text(
                        "${data.firstname} ${data.lastname}",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Spacer(),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: data.description != null
                  ? Text(data.description)
                  : Text("Description"),
            ),
          ],
        ),
      ),
    );
  }

  doctorDetails() {
    HttpHelper.doctorDetails(widget.id).then((response) {
      print(response.body);
      var l =
          JSON.jsonDecode(response.body)['data'].cast<Map<String, dynamic>>();
      List<DoctorDetailsData> list = l
          .map<DoctorDetailsData>((json) => DoctorDetailsData.fromJson(json))
          .toList();

      setState(() {
        this.list = list;
      });
    }).catchError((error) {});
  }
}
