import 'dart:convert';

import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:dermatologist/util/globaldata.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key key}) : super(key: key);

  @override
  _MyForgotPassword createState() => _MyForgotPassword();
}

class _MyForgotPassword extends State<ForgotPassword> {
  TextEditingController emailController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _load = false;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setPrefrenceData();
    HttpHelper.getAdminId();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = _load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

    return Scaffold(
        key: _scaffoldKey,
        body: ListView(
          children: <Widget>[
            new Container(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        child: new Image.asset(
                          'assets/images/Image_forgotpassword.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      Align(
                        child: loadingIndicator,
                        alignment: FractionalOffset.center,
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      "Dermatologist",
                      style: TextStyle(
                          color: HexColor(topBackColor),
                          fontSize: 40,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 30, right: 30, top: 15),
                    child: Column(
                      children: <Widget>[
                        TextField(
                            controller: emailController,
                            decoration: InputDecoration(
                                hintText: "E-mail address",
                                hintStyle: TextStyle(color: Colors.grey),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Color(0xFF81D4FA)),
                                    borderRadius: BorderRadius.circular(10)),
                                prefixIcon: Icon(
                                  Icons.email,
                                  color: AppColors.blue,
                                ))),
                        Padding(
                          padding: const EdgeInsets.only(top: 12.0),
                          child: Text(
                            "Enter your email and will send",
                            style: TextStyle(color: AppColors.grey),
                          ),
                        ),
                        Text("you instructions on how to reset it",
                            style: TextStyle(color: AppColors.grey)),
                        SizedBox(
                          height: 40,
                          width: 0,
                        ),
                        new Container(
                          width: double.infinity,
                          margin: EdgeInsets.symmetric(
                              vertical: 1.0, horizontal: 20),
                          child: ButtonTheme(
                            height: 60,
                            child: RaisedButton(
                              shape: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: HexColor(topBackColor))),
                              child: Text(
                                "Reset Password",
                                style: TextStyle(color: Colors.white),
                              ),
                              onPressed: () {
                                SystemChannels.textInput
                                    .invokeMethod('TextInput.hide');
                                checkInternetConnection(context);
                              },
                              color: HexColor(topBackColor),
                            ),
                          ),
                        ),
                        InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text("Back to Login",
                                  style: TextStyle(color: AppColors.blue)),
                            )),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ));
  }

  checkInternetConnection(BuildContext context) async {
    if (await HttpHelper.isConnectedNew(context, checkInternetConnection,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, checkInternetConnection,
            barrierDismissible: true)) {
      forgotPassword();
    } else {
      buildSnackbar("Please connect to internet");
    }
  }

  void forgotPassword() {
    String email = emailController.text;
    if (email.length == 0) {
      buildSnackbar("Please Enter Email");
    } else if (!GlobalData.isEmailValid(email)) {
      buildSnackbar("Please Enter Valid Email Address");
    } else {
      setState(() {
        _load = true;
      });

      HttpHelper.forgotPassword(email).then((response) {
        print(response.body);
        var jsonParse = json.decode(response.body);
        if (jsonParse['success']) {
          buildSnackbar(jsonParse['message']);
          setState(() {
            _load = false;
          });
          Future.delayed(const Duration(milliseconds: 1500), () {
            Navigator.of(context).pop();
          });
//          Navigator.push(
//              context, MaterialPageRoute(builder: (context) => HomeScreen()));
        } else {
          setState(() {
            _load = false;
          });
          buildSnackbar(jsonParse['message']);
        }
      }).catchError((error) {});
    }
  }

  buildSnackbar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: HexColor(topTextColor)),
      ),
      backgroundColor: HexColor(topBackColor),
    ));
  }

  buildSnackbarwithLoader() {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      duration: new Duration(seconds: 4),
      content: new Row(
        children: <Widget>[
          new CircularProgressIndicator(),
          new Text("Password Reseting...")
        ],
      ),
    ));
  }

  _handleSignIn() {}
}
