import 'dart:convert';
import 'dart:convert' as JSON;

import 'package:dermatologist/model/CasesData.dart';
import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/ui/CaseDetails.dart';
import 'package:dermatologist/ui/EditCase.dart';
import 'package:dermatologist/ui/InstructionScreen.dart';
import 'package:dermatologist/ui/NewCasePage.dart';
import 'package:dermatologist/util/DBProvider.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

class HomeScreen extends StatefulWidget {
  final UserModel userModel;
  UserData userData;

  HomeScreen(this.userModel, this.userData);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var isInsert = false;
  List<CasesData> caseList = new List();

  //List<CategoryData> _categoryList = new List();
  bool _load = false;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF",
      app_id = "",
      user_id = "";

  @override
  void initState() {
    setPrefrenceData();
    getCases();
    // TODO: implement initState
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();
    SharedPref.getAdminId().then((id) {
      setState(() {
        app_id = id;
      });
    });

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });

    _sharedPref.getUserId().then((id) {
      setState(() {
        user_id = id;
      });
    });

    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isInsert) {
      getCaseData();
    }

    double screen_height = MediaQuery.of(context).size.height;

    Widget loadingIndicator = _load
        ? new Container()
        : Center(
          child: new Container(
              margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 10.0,
                      color: Colors.black.withOpacity(.3),
                      offset: Offset(6.0, 7.0),
                    ),
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Colors.white),
              width: 150.0,
              height: 100.0,
              child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new CircularProgressIndicator(),
                    SizedBox(height: 10.0,),
                    Text("Process",style: TextStyle(fontSize: 25.0),)
                  ],
                ),
              ),
            ),
        );
    return Scaffold(
        body: Container(
      child: new Container(
        color: Color(0xFFFAFAFA),
        margin: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
        child: _load
            ? caseList.length > 0
                ? new Container(
                    child: new ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: caseList.length,
                      itemBuilder: (context, i) {
                        return _buildRow(caseList[i], i);
                      },
                    ),
                  )
                : new Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Expanded(
                            child: Align(
                          alignment: FractionalOffset.center,
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: screen_height / 3 - 30.0,
                                width: 220,
                                child: InkWell(
                                  child: Card(
                                    child: Center(
                                      child: Icon(
                                        Icons.format_align_left,
                                        size: 60,
                                        color: AppColors.blue,
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    instructionPage();
                                  },
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4),
                                child: Text(
                                  "View Instructions",
                                  style: TextStyle(
                                      fontSize: 18, color: AppColors.blue),
                                ),
                              )
                            ],
                          ),
                        )),
                        Expanded(
                            child: Align(
                          alignment: FractionalOffset.center,
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: screen_height / 3 - 30.0,
                                width: 220,
                                child: InkWell(
                                  child: Card(
                                    child: Center(
                                      child: Icon(
                                        Icons.not_listed_location,
                                        size: 60,
                                        color: AppColors.blue,
                                      ),
                                    ),
                                  ),
                                  onTap: () {
                                    sendQuestion();
                                  },
                                ),
                              ),
                              Container(
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Text(
                                    "Send a question to the \nDermatologist",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: AppColors.blue,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )),
                      ],
                    ),
//                      margin: EdgeInsets.fromLTRB(0.0, 50, 0.0, 10),
                  )
            : Align(
                child: loadingIndicator,
                alignment: FractionalOffset.center,
              ),
      ),
    ));
  }

  Widget _buildRow(CasesData data, int index) {
    return new ListTile(
      onTap: () {
        _CaseDetails(data);
      },
      title: Container(
        decoration: new BoxDecoration(
            borderRadius: BorderRadius.all(new Radius.circular(0))),
        margin: EdgeInsets.symmetric(vertical: 1, horizontal: 0),
        child: Card(
          child: new Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                data.is_offline.contains("1")
                    ? Container(
                        padding: EdgeInsets.all(5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            new Text(
                              "Draft",
                              style: TextStyle(
                                  color: Colors.green,
                                  fontWeight: FontWeight.w700),
                              textAlign: TextAlign.right,
                            )
                          ],
                        ),
                      )
                    : Container(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    new Flexible(
                        child: Container(
                      child: new Text(
                        data.Name,
                        overflow: TextOverflow.clip,
                        style: TextStyle(
                          color: HexColor(topBackColor),
                          fontWeight: FontWeight.w500,
                          fontSize: 17,
                        ),
                      ),
                    )),
                    Spacer(),
                    Text("${data.date} \n ${data.time}",
                        textAlign: TextAlign.right),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Row(
                    children: <Widget>[
                      Text(
                        "Case Number: ",
                        style: TextStyle(color: AppColors.blue),
                      ),
                      Text(data.case_id),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Row(
                    children: <Widget>[
                      Text("Transaction ID: ",
                          style: TextStyle(
                            color: AppColors.blue,
                          )),
                      Expanded(
                        child: Text(data.transaction_id),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8),
                  child: Row(
                    children: <Widget>[
                      Text("Amount: ", style: TextStyle(color: AppColors.blue)),
                      Text(data.amount)
                    ],
                  ),
                )
              ],
            ),
            padding: EdgeInsets.all(10),
          ),
          elevation: 3.0,
        ),
      ),
    );
  }

  sendQuestion() async {
    var retunData = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) =>
                new NewCasePage(true, widget.userModel, widget.userData)));

    getCases();
  }

  instructionPage() async {
    var retunData = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) =>
                new InstructionPage(widget.userModel, widget.userData)));

//    getCases();
  }

  getCases() async {
    if (await HttpHelper.isConnectedNew(context, getCases,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, getCases,
            barrierDismissible: true)) {
      HttpHelper.getCases(widget.userModel.user_id).then((response) {
        var jsonParse = json.decode(response.body);
        if (jsonParse['status']) {
          var l = JSON
              .jsonDecode(response.body)['data']
              .cast<Map<String, dynamic>>();
          print(response.body);
          List<CasesData> caselist =
              l.map<CasesData>((json) => CasesData.fromJson(json)).toList();
          setState(() {
            _load = true;
            caseList = caselist;
            _checkCaseData();
          });
//
        } else {
          setState(() {
            _load = true;
          });
        }
      }).catchError((error) {
        setState(() {
          _load = true;
        });
      });
    } else {
      getCaseData();
    }
  }

  _insertCategoryData() async {
    Database db = await DBProvider.instance.database;
    int i = 0;
    for (CasesData data in caseList) {
      i = i + 1;
      Map<String, dynamic> row = {
        DBProvider.admin_id: app_id,
        DBProvider.user_id: user_id,
        DBProvider.id: data.id,
        DBProvider.case_id: data.case_id,
        DBProvider.photos: data.photos.toString(),
        DBProvider.problem_occur: data.problem_occur,
        DBProvider.Name: data.Name,
        DBProvider.contact: data.contact,
        DBProvider.sex: data.sex,
        DBProvider.description: data.description,
        DBProvider.transaction_id: data.transaction_id,
        DBProvider.amount: data.amount,
        DBProvider.status: "1",
        DBProvider.date: data.date,
        DBProvider.time: data.time,
        DBProvider.is_offline: data.is_offline
      };
      await db.insert(DBProvider.case_list, row);
    }
    getCaseData();
  }

  _checkCaseData() async {
    Database db = await DBProvider.instance.database;

    List<Map> results = await db.query(DBProvider.case_list,
        columns: [DBProvider.id, DBProvider.case_id],
        where: '${DBProvider.admin_id} = ? AND ${DBProvider.user_id} =  ?',
        whereArgs: [app_id, user_id]);

    if (results.length > 0) {
      _deleteCaseData();
    } else {
      _insertCategoryData();
    }
  }

  _deleteCaseData() async {
    Database db = await DBProvider.instance.database;

    int data = await db.rawDelete(
        'DELETE FROM ${DBProvider.case_list} WHERE ${DBProvider.admin_id} = ${app_id} AND ${DBProvider.user_id} = ${user_id} AND ${DBProvider.is_offline} = "0" ');

    _insertCategoryData();
  }

  void _CaseDetails(CasesData data) {
    if (data.is_offline.contains("0")) {
      Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (BuildContext context) => new CaseDetails(data)));
    } else {
      Navigator.push(
          context,
          new MaterialPageRoute(
              builder: (BuildContext context) =>
                  new EditCase(data, widget.userModel, widget.userData)));
    }
  }

  void getCaseData() async {
    Database db = await DBProvider.instance.database;

    List<Map> results = await db.query(DBProvider.case_list,
        where: '${DBProvider.admin_id} = ? AND ${DBProvider.user_id} =  ?',
        whereArgs: [app_id, user_id]);

    print(results.length);

    List<CasesData> tmp_caseList = new List();

    for (int i = 0; i < results.length; i++) {
      List<dynamic> photos = new List();

      if (results[i][DBProvider.photos].toString().isNotEmpty) {
        String photoData = results[i][DBProvider.photos]
            .substring(1, results[i][DBProvider.photos].toString().length - 1);
        for (int i = 0; i < photoData.split(",").length; i++) {
          photos.add(photoData.split(",")[i]);
        }
      }

      List<CasesData> arrCaseList = new List();

      arrCaseList.add(new CasesData(
          app_id,
          results[i][DBProvider.id],
          results[i][DBProvider.Name],
          photos,
          results[i][DBProvider.sex],
          results[i][DBProvider.user_id],
          results[i][DBProvider.contact],
          results[i][DBProvider.description],
          results[i][DBProvider.date],
          results[i][DBProvider.case_id],
          results[i][DBProvider.time],
          results[i][DBProvider.problem_occur],
          results[i][DBProvider.transaction_id],
          results[i][DBProvider.amount],
          results[i][DBProvider.is_offline]));

      if (arrCaseList[0].is_offline.contains("1")) {
        tmp_caseList.insert(0, arrCaseList[0]);
      } else {
        tmp_caseList.add(arrCaseList[0]);
      }
    }

    setState(() {
      _load = true;
      caseList = tmp_caseList;
    });
  }
}
