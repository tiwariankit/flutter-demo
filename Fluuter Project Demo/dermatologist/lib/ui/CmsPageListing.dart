import 'dart:convert' as JSON;

import 'package:dermatologist/model/CmsListData.dart';
import 'package:dermatologist/util/DBProvider.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqlite_api.dart';

import 'CMSPages.dart';

class CmsPageListing extends StatefulWidget {
  CmsPageListing({Key key}) : super(key: key);

  @override
  _MyCmsPageListing createState() => _MyCmsPageListing();
}

class _MyCmsPageListing extends State<CmsPageListing> {
  int _currentIndex = 0;
  bool _load = false;

  //var list=new List<DoctorListData>();
  List<CmsListData> list = new List();

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF",
      app_id = "";

  @override
  void initState() {
    // TODO: implement initState
    setPrefrenceData();
    getcmsListing();
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    SharedPref.getAdminId();

    SharedPref.getAdminId().then((id) {
      setState(() {
        app_id = id;
      });
    });

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = !_load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: HexColor(topBackColor),
        actions: <Widget>[
          Container(
            color: HexColor(topBackColor),
          )
        ],
        leading: new IconButton(
            icon: new Icon(Icons.chevron_left, color: HexColor(topTextColor)),
            onPressed: () => Navigator.of(context).pop("Hi Helloooo")),
        title: Text(
          "Information",
          style: TextStyle(color: HexColor(topTextColor)),
        ),
      ),
      body: _load
          ? ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
              scrollDirection: Axis.vertical,
              itemCount: list.length,
              itemBuilder: (context, i) {
                // If you've reached at the end of the available word pairs...
                return _buildRow(list[i], i);
              },
            )
          : Align(
              child: loadingIndicator,
              alignment: FractionalOffset.center,
            ),
    );
  }

  Widget _buildRow(CmsListData data, int index) {
    return new ListTile(
      onTap: () {
        _cmsDetails(data.id);
      },
      title: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(
                  Icons.info_outline,
                  size: 20,
                  color: HexColor(topBackColor),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Column(
                    children: <Widget>[
                      Text(
                        data.title,
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Spacer(),
                Icon(
                  Icons.chevron_right,
                  color: AppColors.grey,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  getcmsListing() async {
    if (await HttpHelper.isConnectedNew(context, getcmsListing,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, getcmsListing,
            barrierDismissible: true)) {
      HttpHelper.getCmsListing().then((response) {
        print(response.body);
        var l =
            JSON.jsonDecode(response.body)['data'].cast<Map<String, dynamic>>();
        List<CmsListData> list =
            l.map<CmsListData>((json) => CmsListData.fromJson(json)).toList();
        setState(() {
          this.list = list;
          _load = true;
          _checkCMSListData();
        });
      }).catchError((error) {
        setState(() {
          _load = true;
        });
      });
    } else {
      new Future.delayed(const Duration(seconds: 1), () {
        _getOfflineCMSDATA();
      });
    }
  }

  _getOfflineCMSDATA() async {
    Database db = await DBProvider.instance.database;

    List<Map> results = await db.query(DBProvider.table_cmsListData,
        where: '${DBProvider.cms_admin_id} = ? ', whereArgs: [app_id]);
    List<CmsListData> tmp_cmsList = new List();

    if (results.length > 0) {
      for (int i = 0; i < results.length; i++) {
        tmp_cmsList.add(new CmsListData(
          results[i][DBProvider.cms_listid],
          results[i][DBProvider.cms_listtitle],
          results[i][DBProvider.cms_listadmin_id],
        ));
      }
      setState(() {
        _load = true;
        this.list = tmp_cmsList;
      });
    } else {
      setState(() {
        _load = true;
      });
    }
  }

  _checkCMSListData() async {
    Database db = await DBProvider.instance.database;

    List<Map> results = await db.query(DBProvider.table_cmsListData,
        where: '${DBProvider.cms_listadmin_id} = ?', whereArgs: [app_id]);

    if (results.length > 0) {
      _deleteCmsListData();
    } else {
      _insertCmsListData();
    }
  }

  _deleteCmsListData() async {
    Database db = await DBProvider.instance.database;

    int data = await db.rawDelete(
        'DELETE FROM ${DBProvider.table_cmsListData} WHERE ${DBProvider.cms_listadmin_id} = ${app_id}');
    print(data);
    _insertCmsListData();
  }

  _insertCmsListData() async {
    Database db = await DBProvider.instance.database;

    for (CmsListData data in list) {
      Map<String, dynamic> row = {
        DBProvider.cms_listadmin_id: data.admin_id,
        DBProvider.cms_listid: data.id,
        DBProvider.cms_title: data.title,
      };
      await db.insert(DBProvider.table_cmsListData, row);
    }
  }

  void _cmsDetails(String id) {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new CMSPages(id)));
  }
}
