import 'dart:convert';
import 'dart:convert' as JSON;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dermatologist/model/CasesData.dart';
import 'package:dermatologist/model/PrescripationData.dart';
import 'package:dermatologist/ui/full_imageview.dart';
import 'package:dermatologist/util/DBProvider.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:sqflite/sqlite_api.dart';

import 'CommentView.dart';

class CaseDetails extends StatefulWidget {
  CasesData casedataObj;

  CaseDetails(this.casedataObj);

  @override
  _MyCaseDetails createState() => _MyCaseDetails();
}

class _MyCaseDetails extends State<CaseDetails> {
  var isLoadData = false;
  int _currValue = 0;
  bool isDays = true, isWeeks = false, isMonth = false, isYears = false;
  String text = "";
  List<PrescriptionData> prescriptionData = new List();
  bool isPrescriptionData = false;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF",
      app_id = "",
      user_id = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setPrefrenceData();
    getPrescriptionData();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();

    _sharedPref.getUserId().then((id) {
      setState(() {
        user_id = id;
      });
    });
    SharedPref.getAdminId().then((id) {
      setState(() {
        app_id = id;
      });
    });

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: HexColor(topBackColor),
          actions: <Widget>[
            Container(
              color: HexColor(topBackColor),
            )
          ],
          leading: new IconButton(
            icon: new Icon(Icons.chevron_left, color: HexColor(topTextColor)),
            onPressed: () => Navigator.of(context).pop("Hi Helloooo"),
          ),
          title: Text(
            "Case Details",
            style: TextStyle(color: HexColor(topTextColor)),
          ),
        ),
        body: Container(
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Name:",
                      style: TextStyle(fontSize: 20, color: AppColors.blue),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 50),
                        child: Text(widget.casedataObj.Name,
//                          overflow: TextOverflow.clip,
                            style: TextStyle(fontSize: 20)),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Contact:",
                      style: TextStyle(fontSize: 20, color: AppColors.blue),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Text(widget.casedataObj.contact,
                          overflow: TextOverflow.clip,
                          style: TextStyle(fontSize: 20)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Sex:",
                      style: TextStyle(fontSize: 20, color: AppColors.blue),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 70),
                      child: widget.casedataObj.sex == "1"
                          ? Text("Male", style: TextStyle(fontSize: 20))
                          : Text("Female", style: TextStyle(fontSize: 20)),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "How long have you had this conditions?",
                      style: TextStyle(fontSize: 19, color: AppColors.blue),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 14),
                      child: _problem_data(widget.casedataObj.problem_occur),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Description:",
                      style: TextStyle(fontSize: 20, color: AppColors.blue),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 14),
                      child: Text(widget.casedataObj.description,
                          style: TextStyle(fontSize: 14)),
                    )
                  ],
                ),
              ),
              Visibility(
                visible: false,
                child: Container(
                  height: 1,
                  color: AppColors.grey,
                ),
              ),
              Visibility(
                visible: true,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: isPrescriptionData
                      ? Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Text(
                                  "Dermatologist Comment",
                                  style: TextStyle(
                                      fontSize: 20, color: AppColors.blue),
                                ),
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 14),
                              child: new Html(
                                data: prescriptionData.length > 0
                                    ? prescriptionData[0].prescription
                                    : "",
                                defaultTextStyle: TextStyle(fontSize: 14),
                              ),
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: InkWell(
                                    onTap: () => {_openChatWindow()},
                                    child: Text(
                                      "Reply",
                                      style: TextStyle(
                                          color: AppColors.blue,
                                          fontWeight: FontWeight.w600),
                                    ),
                                  ),
                                ))
                          ],
                        )
                      : new Container(
                          child: Text(""),
                        ),
                ),
              ),
              Container(
                height: 1,
                color: AppColors.grey,
              ),
              Visibility(
                visible: widget.casedataObj.photos != null &&
                        widget.casedataObj.photos.length > 0
                    ? true
                    : false,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Picture:",
                        style: TextStyle(fontSize: 20, color: AppColors.blue),
                      ),
                      new Container(
                        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                        height: 100,
                        child: new ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: widget.casedataObj.photos.length,
                          itemBuilder: (context, j) {
                            return _buildAnswerRow(
                                widget.casedataObj.photos[j]);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget _buildAnswerRow(String answer) {
    return InkWell(
      onTap: () => {openImageView(answer)},
      child: new Container(
        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                blurRadius: 7.0,
                color: Colors.black.withOpacity(0.3),
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(5)),
            color: Colors.white),
        width: 100,
        child: ClipRRect(
          borderRadius: new BorderRadius.circular(5.0),
          child: Stack(
            children: <Widget>[
              Center(
                  child: CircularProgressIndicator(
                      backgroundColor: HexColor(topBackColor))),
              CachedNetworkImage(
                imageUrl: answer.trim(),
                height: 100,
                width: 100,
                fit: BoxFit.fill,
              ),
//              new Image.network(
//                answer.trim(),
//                height: 100,
//                width: 100,
//                fit: BoxFit.fill,
//              )
            ],
          ),
        ),
      ),
    );
  }

  void openImageView(String ans) {
    print(ans);

    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new FullImageView(ans.trim())));
  }

  Widget _problem_data(String problem_occur) {
    if (problem_occur == "1") {
      return Text("Days", style: TextStyle(fontSize: 20));
    } else if (problem_occur == "2") {
      return Text("Weeks", style: TextStyle(fontSize: 20));
    } else if (problem_occur == "3") {
      return Text("Months", style: TextStyle(fontSize: 20));
    } else if (problem_occur == "4") {
      return Text("Years", style: TextStyle(fontSize: 20));
    }
  }

  void getPrescriptionData() async {
    if (await HttpHelper.isConnectedNew(context, getPrescriptionData,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, getPrescriptionData,
            barrierDismissible: true)) {
      HttpHelper.getPrescription(widget.casedataObj.id).then((response) {
        var jsonParse = json.decode(response.body);
        print(jsonParse);
        if (jsonParse['status']) {
          var l = JSON
              .jsonDecode(response.body)['data']
              .cast<Map<String, dynamic>>();
          List<PrescriptionData> list = l
              .map<PrescriptionData>((json) => PrescriptionData.fromJson(json))
              .toList();
          setState(() {
            isLoadData = true;
            prescriptionData = list;
            isPrescriptionData = true;

            _checkPrescriptionData();
          });
        } else {
          isPrescriptionData = false;
        }
      }).catchError((error) {});
    } else {
      getPrescriptionDataOffline();
    }
  }

  getPrescriptionDataOffline() async {
    Database db = await DBProvider.instance.database;

    List<Map> results = await db.query(DBProvider.table_prescription_data,
        where:
            '${DBProvider.pres_admin_id} = ? AND ${DBProvider.pres_user_id} =  ? AND ${DBProvider.pres_case_id} = ?',
        whereArgs: [app_id, user_id, widget.casedataObj.case_id]);

    List<PrescriptionData> tmp_data = new List();

    for (int i = 0; i < results.length; i++) {
      tmp_data
          .add(new PrescriptionData(results[i][DBProvider.pres_data], "", ""));
    }
    setState(() {
      isLoadData = true;
      prescriptionData = tmp_data;
      isPrescriptionData = true;
    });
  }

  _checkPrescriptionData() async {
    Database db = await DBProvider.instance.database;

    List<Map> results = await db.query(DBProvider.table_prescription_data,
        where:
            '${DBProvider.pres_admin_id} = ? AND ${DBProvider.pres_user_id} =  ? AND ${DBProvider.pres_case_id} = ?',
        whereArgs: [app_id, user_id, widget.casedataObj.case_id]);

    if (results.length > 0) {
      _deletePrescriptionData();
    } else {
      _insertCategoryData();
    }
  }

  _deletePrescriptionData() async {
    Database db = await DBProvider.instance.database;

    int data = await db.rawDelete(
        'DELETE FROM ${DBProvider.table_prescription_data} WHERE ${DBProvider.pres_admin_id} = ${app_id} AND ${DBProvider.pres_user_id} = ${user_id} AND ${DBProvider.pres_case_id} = ${widget.casedataObj.id}');
    _insertCategoryData();
  }

  _insertCategoryData() async {
    Database db = await DBProvider.instance.database;
    for (PrescriptionData data in prescriptionData) {
      Map<String, dynamic> row = {
        DBProvider.pres_admin_id: app_id,
        DBProvider.pres_user_id: user_id,
        DBProvider.pres_case_id: widget.casedataObj.id,
        DBProvider.pres_data: data.prescription
      };
      await db.insert(DBProvider.table_prescription_data, row);
    }
  }

  _openChatWindow() {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) =>
                new CommentViewPage(widget.casedataObj.id)));
  }
}
