import 'dart:convert' as JSON;

import 'package:dermatologist/model/ClinicData.dart';
import 'package:dermatologist/ui/ClinicDetails.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

class ClinicPharmacies extends StatefulWidget {
  ClinicPharmacies({Key key}) : super(key: key);

  @override
  _MyClinicPharmacies createState() => _MyClinicPharmacies();
}

class _MyClinicPharmacies extends State<ClinicPharmacies> {
  bool _load = false;

  //var list=new List<DoctorListData>();
  List<ClinicData> clinicList = new List();

  //Map<String, double> userLocation;
  var currentLocation;


  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";



  @override
  void initState() {
    // TODO: implement initState
    doctorList();
    setPrefrenceData();
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();
    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });

  }


  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = !_load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: HexColor(topBackColor),
        actions: <Widget>[
          Container(
            color: HexColor(topBackColor),
          )
        ],
        title: Text(
          "Clinic & Pharmacies",
          style: TextStyle(color: HexColor(topTextColor)),
        ),
        leading: new IconButton(
            icon: new Icon(Icons.chevron_left, color: HexColor(topTextColor)),
            onPressed: () => Navigator.of(context).pop("Hi Helloooo")),
      ),
      body: _load
          ? ListView.separated(
              separatorBuilder: (context, index) => Divider(
                color: Colors.black,
              ),
              scrollDirection: Axis.vertical,
              itemCount: clinicList.length,
              itemBuilder: (context, i) {
                // If you've reached at the end of the available word pairs...
                return _buildRow(clinicList[i], i);
              },
            )
          : Align(
              child: loadingIndicator,
              alignment: FractionalOffset.center,
            ),
    );
  }

  Widget _buildRow(ClinicData data, int index) {
    return new ListTile(
      title: InkWell(
        onTap: () => {openDetailPage(data)},
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    data.name,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  new IconButton(
                    icon: new Icon(Icons.chevron_right, color: Colors.blue),
                    onPressed: () {},
                  )
                ],
              ),
              Text(
                data.location,
                overflow: TextOverflow.clip,
              )
            ],
          ),
        ),
      ),
    );
  }

  openDetailPage(ClinicData data) {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new ClinicDetail(data)));
  }

  doctorList() async {
    var geolocator = Geolocator();
    Position position = await geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    getDoctorListData(
        position.latitude.toString(), position.longitude.toString());
  }

  getDoctorListData(String lat, String long) async {
    HttpHelper.ClinicList(lat, long).then((response) {
      print(response.body);
      var l = JSON
          .jsonDecode(response.body)['records']
          .cast<Map<String, dynamic>>();
      List<ClinicData> list =
          l.map<ClinicData>((json) => ClinicData.fromJson(json)).toList();

      setState(() {
        this.clinicList = list;
        _load = true;
      });
    }).catchError((error) {});
  }
}
