import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/ui/DoctorList.dart';
import 'package:dermatologist/ui/EditProfile.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'CmsPageListing.dart';
import 'LoginScreen.dart';

class MorePage extends StatefulWidget {
  final UserData userData;
  final UserModel _userModel;

  MorePage(this.userData, this._userModel);

  @override
  _MyMorePage createState() => _MyMorePage();
}

class _MyMorePage extends State<MorePage> {
  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setPrefrenceData();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      home: Scaffold(
          body: new Container(
        child: Column(
          children: <Widget>[
            InkWell(
              onTap: _OpenCMsPage,
              child: Padding(
                padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.info_outline,
                      size: 25,
                      color: HexColor(topBackColor),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text(
                        "Information",
                        style: TextStyle(fontSize: 17),
                      ),
                    ),
                    Visibility(
                      visible: false,
                      child: Icon(
                        Icons.chevron_right,
                        color: AppColors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 1,
              color: AppColors.grey,
            ),
            InkWell(
              onTap: () {
                editProfile();
              },
              child: Padding(
                padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.mode_edit,
                      size: 25,
                      color: HexColor(topBackColor),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text(
                        "My Profile",
                        style: TextStyle(fontSize: 17),
                      ),
                    ),
                    Visibility(
                      visible: false,
                      child: Icon(
                        Icons.chevron_right,
                        color: AppColors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 1,
              color: AppColors.grey,
            ),
            InkWell(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (context) => new AlertDialog(
                    content: new Text('Are you sure want to logout?'),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () => Navigator.of(context).pop(false),
                        child: new Text('No'),
                      ),
                      new FlatButton(
                        onPressed: () => logoutData(),
                        child: new Text('Yes'),
                      ),
                    ],
                  ),
                );
              },
              child: Padding(
                padding: EdgeInsets.fromLTRB(15, 10, 0, 10),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.exit_to_app,
                      size: 25,
                      color: HexColor(topBackColor),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 15),
                      child: Text(
                        "Logout",
                        style: TextStyle(fontSize: 17),
                      ),
                    ),
                    Visibility(
                      visible: false,
                      child: Icon(
                        Icons.chevron_right,
                        color: AppColors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 1,
              color: AppColors.grey,
            ),
          ],
        ),
      )),
    );
  }

  doctorList() {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new DoctorList()));
  }

  _OpenCMsPage() {
    Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) => new CmsPageListing()));
  }

  logoutData() {
    new Future.delayed(const Duration(milliseconds: 500), () {
      Fluttertoast.showToast(
          msg: "Successfully logout",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: HexColor(topBackColor),
          textColor: HexColor(topTextColor),
          fontSize: 16.0);
    });

    widget.userData.signOut();

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => new LoginScreen(widget.userData)),
          (Route<dynamic> route) => false,
    );

//    Navigator.of(context).pushReplacement(new MaterialPageRoute(
//        builder: (context) => new LoginScreen(widget.userData)));
  }

  editProfile() {
    Navigator.of(context).push(new MaterialPageRoute(
        builder: (context) =>
            new EditProfile(widget.userData.getUser(), widget.userData)));
  }
}
