import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/ui/NewCasePage.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:page_indicator/page_indicator.dart';

class InstructionPage extends StatefulWidget {
  UserModel userModel;
  UserData userData;

  InstructionPage(this.userModel, this.userData);

  @override
  _MyInstructionPage createState() => _MyInstructionPage();
}

class _MyInstructionPage extends State<InstructionPage> {
  int _currentIndex = 0;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";

  @override
  void initState() {
    // TODO: implement initState
    setPrefrenceData();
    super.initState();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          backgroundColor: HexColor(topBackColor),
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: HexColor(topTextColor)),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Row(
            children: <Widget>[
              new Text(
                "Instructions",
                style: TextStyle(color: HexColor(topTextColor)),
              )
            ],
          )),
      body: new Stack(
        children: <Widget>[
          new Container(
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 60),
            child: PageIndicatorContainer(
              indicatorColor: AppColors.blue,
              indicatorSelectorColor: HexColor(topBackColor),
              shape: IndicatorShape.circle(size: 10),
              child: PageView(
                pageSnapping: true,
                physics: BouncingScrollPhysics(),
                children: <Widget>[
                  Container(
                    child: Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          "SNAP",
                          style: TextStyle(
                              fontSize: 25,
                              color: AppColors.blue,
                              fontWeight: FontWeight.w700),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Text(
                            "Take two photos of your skin concern",
                            style: TextStyle(
                                fontSize: 18,
                                color: HexColor(topBackColor),
                                fontWeight: FontWeight.w700),
                          ),
                        )
                      ],
                    )),
                    color: Colors.white,
                  ),
                  Container(
                    child: Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          "SEND",
                          style: TextStyle(
                              fontSize: 25,
                              color: AppColors.blue,
                              fontWeight: FontWeight.w700),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Text(
                            "Fill out your information send to our dermatologists",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: HexColor(topBackColor),
                                fontWeight: FontWeight.w700),
                          ),
                        )
                      ],
                    )),
                    color: Colors.white,
                  ),
                  Container(
                    child: Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          "REVIEW",
                          style: TextStyle(
                              fontSize: 25,
                              color: AppColors.blue,
                              fontWeight: FontWeight.w700),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Text(
                            "A doctor will send you an answer within hours",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 18,
                                color: HexColor(topBackColor),
                                fontWeight: FontWeight.w700),
                          ),
                        )
                      ],
                    )),
                    color: Colors.white,
                  ),
                  Container(
                    child: Center(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text(
                          "VISIT",
                          style: TextStyle(
                              fontSize: 25,
                              color: AppColors.blue,
                              fontWeight: FontWeight.w700),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Text(
                            "Find health care centers near you",
                            style: TextStyle(
                                fontSize: 18,
                                color: HexColor(topBackColor),
                                fontWeight: FontWeight.w700),
                          ),
                        )
                      ],
                    )),
                    color: Colors.white,
                  )
                ],
                scrollDirection: Axis.horizontal,
              ),
              length: 4,
            ),
          ),
          giveMeSuggestionButton()
        ],
      ),
    );
  }

  Widget giveMeSuggestionButton() {
    return Align(
        alignment: FractionalOffset.bottomCenter,
        child: Stack(
          children: <Widget>[
            Container(
              color: HexColor(topBackColor),
              height: 60,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new InkWell(
                    child: new Container(
                      margin:
                          EdgeInsets.symmetric(vertical: 0.0, horizontal: 30.0),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 17, 0, 19),
                        child: new Text("Open a new case",
                            style: TextStyle(
                                color: HexColor(topTextColor),
                                fontSize: 20,
                                fontWeight: FontWeight.w600)),
                      ),
                    ),
                    onTap: () => {openNewCases()},
                  ),
                ],
              ),
            )
          ],
        ));
  }

  openNewCases() async {
    var retunData = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (BuildContext context) =>
                new NewCasePage(true, widget.userModel, widget.userData)));
  }
}
