import 'dart:async';
import 'dart:convert';
import 'dart:convert' as JSON;
import 'dart:io';

import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/ui/DashBoardScreen.dart';
import 'package:dermatologist/ui/ForgotPassword.dart';
import 'package:dermatologist/ui/RegistrationPage.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:dermatologist/util/globaldata.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:page_transition/page_transition.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen(this.userData);

  final UserData userData;

  @override
  _MyLoginScreen createState() => _MyLoginScreen();
}

class _MyLoginScreen extends State<LoginScreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _load = false;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF",
      deviceType = "",
      deviceToken = "";

  int _state = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    firebaseCloudMessaging_Listeners();
    setPrefrenceData();
//    _fetchCheckData();
  }

  void firebaseCloudMessaging_Listeners() {
    var deviceToken, deviceType = "";
    if (Platform.isAndroid) {
      deviceType = "Android";
    } else {
      deviceType = "iOS";
    }

    final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));

    Stream<String> fcmStream = _firebaseMessaging.onTokenRefresh;
    fcmStream.listen((token) {
      // saveToken(token);
      deviceToken = token;
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {},
      onResume: (Map<String, dynamic> message) async {},
      onLaunch: (Map<String, dynamic> message) async {},
    );

    print("Login Token $deviceToken");
    SharedPref sharedPref = new SharedPref();
    sharedPref.notificationSettingData(deviceType, deviceToken);
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();
    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });

    _sharedPref.getDeviceType().then((type) {
      setState(() {
        deviceType = type;
      });
    });

    _sharedPref.getDeviceToken().then((token) {
      setState(() {
        deviceToken = token;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = _load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();
    return Scaffold(
        key: _scaffoldKey,
        body: ListView(
          children: <Widget>[
            new Container(
              child: Column(
                children: <Widget>[
                  Stack(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        child: new Image.asset(
                          'assets/images/ImageLogin.jpg',
                          fit: BoxFit.cover,
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      "Dermatologist",
                      style: TextStyle(
                          color: HexColor(topBackColor),
                          fontSize: 40,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 30, right: 30),
                    child: Column(
                      children: <Widget>[
                        TextField(
                            controller: emailController,
                            decoration: InputDecoration(
                                hintText: "E-mail address",
                                hintStyle: TextStyle(color: Colors.grey),
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Color(0xFF81D4FA)),
                                    borderRadius: BorderRadius.circular(10)),
                                prefixIcon: Icon(
                                  Icons.email,
                                  color: AppColors.blue,
                                ))),
                        Padding(
                          padding: const EdgeInsets.only(top: 15),
                          child: TextField(
                              controller: passwordController,
                              obscureText: true,
                              decoration: InputDecoration(
                                  hintStyle: TextStyle(color: Colors.grey),
                                  hintText: "Password",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xFF81D4FA)),
                                      borderRadius: BorderRadius.circular(10)),
                                  prefixIcon: Icon(
                                    Icons.lock,
                                    color: AppColors.blue,
                                  ))),
                        ),
                        new Container(
                          margin: EdgeInsets.fromLTRB(0.0, 10, 0.0, 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ForgotPassword()));
                                },
                                child: Text(
                                  "Forgot password?",
                                  style:
                                      TextStyle(color: HexColor(topBackColor)),
                                ),
                              )
                            ],
                          ),
                        ),
                        new Container(
                          height: 60,
                          width: double.infinity,
                          margin: EdgeInsets.symmetric(
                              vertical: 0.0, horizontal: 10),
                          child: ButtonTheme(
                            child: RaisedButton(
                              shape: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: HexColor(topBackColor))),
                              child: setUpButtonChild(),
                              onPressed: () {
                                SystemChannels.textInput
                                    .invokeMethod('TextInput.hide');
                                checkInternetConnection(context);
                              },
                              color: HexColor(topBackColor),
                            ),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.all(15),
                          child: Column(
                            children: <Widget>[
                              InkWell(
                                  onTap: () {
                                    _openRegisterPage();
                                  },
                                  child: Text(
                                    "Create New Account",
                                    style: TextStyle(
                                        color: HexColor(topBackColor),
                                        fontSize: 15),
                                  ))
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ));
  }

  checkInternetConnection(BuildContext context) async {
    if (await HttpHelper.isConnectedNew(context, checkInternetConnection,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, checkInternetConnection,
            barrierDismissible: true)) {
      userLogin();
    } else {
      buildSnackbar("Please connect to internet");
    }
  }

  _openRegisterPage() {
    Navigator.push(
        context,
        PageTransition(
            type: PageTransitionType.leftToRight, child: Registration()));
  }

  buildSnackbar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: HexColor(topTextColor)),
      ),
      backgroundColor: HexColor(topBackColor),
    ));
  }

  Future userLogin() async {
    String email = emailController.text;
    String password = passwordController.text;

    if (email.trim().length == 0) {
      buildSnackbar("Please Enter Email");
    } else if (!GlobalData.isEmailValid(email.trim())) {
      buildSnackbar("Please Enter Valid Email Address");
    } else if (password.length == 0) {
      buildSnackbar("Please Enter Password");
    } else {
      setState(() {
        _load = true;
        if (_state == 0) {
          animateButton();
        }
      });

      HttpHelper.userLogin(email, password, deviceType, deviceToken)
          .then((response) {
        print(response.body);
        var jsonParse = json.decode(response.body);
        if (jsonParse['status']) {
          UserModel user =
              UserModel.fromJson(JSON.jsonDecode(response.body)['user']);
          setState(() {
            widget.userData.setUser(user);
            _load = false;
            _state = 0;
            new Future.delayed(const Duration(milliseconds: 500), () {
              Fluttertoast.showToast(
                  msg: "Successfully login",
                  toastLength: Toast.LENGTH_SHORT,
                  gravity: ToastGravity.BOTTOM,
                  timeInSecForIos: 1,
                  backgroundColor: HexColor(topBackColor),
                  textColor: HexColor(topTextColor),
                  fontSize: 16.0);
            });

            Navigator.of(context).pushReplacement(new MaterialPageRoute(
                builder: (context) => new DashBoardScreen(
                    widget.userData.getUser(), widget.userData)));
          });
        } else {
          buildSnackbar(jsonParse['message']);
          setState(() {
            _load = false;
            _state = 0;
          });
        }
      }).catchError((error) {
        setState(() {
          _state = 0;
        });
      });
    }
  }

  Widget setUpButtonChild() {
    if (_state == 0) {
      return new Text(
        "Login",
        style: const TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      );
    } else if (_state == 1) {
      return
          CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white)
      );
    }
  }

  void animateButton() {
    setState(() {
      _state = 1;
    });

//    Timer(Duration(milliseconds: 3300), () {
//      setState(() {
//        _state = 2;
//      });
//    });
  }
}
