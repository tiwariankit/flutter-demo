import 'package:dermatologist/util/appcolors.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class FullImageView extends StatefulWidget {
  String imagePath;

  FullImageView(this.imagePath);

  // DoctorDetails({Key key}) : super(key: key);

  @override
  _FullImageViewState createState() => _FullImageViewState();
}

class _FullImageViewState extends State<FullImageView>
{
  double _scale = 1.0;
  double _previousScale = null;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
        body: Stack(
      children: <Widget>[
        new Container(
          alignment: Alignment.center,
          child: PhotoView(
            scaleStateController: PhotoViewScaleStateController(),
            imageProvider:NetworkImage(widget.imagePath),
          ),
          margin: EdgeInsets.fromLTRB(0.0, 60, 0, 0),
        ),
        new Container(
          child: Align(
            alignment: Alignment.topLeft,
            child: new IconButton(
                icon: Image.asset("assets/images/icon.png",height: 40,width: 40,color: AppColors.red,),
                onPressed: () => Navigator.of(context).pop()),
          ),
          height: 40,
          margin: EdgeInsets.fromLTRB(0.0, 20, 0.0, 0),
        ),
      ],
    ));
  }
}
