import 'dart:async';
import 'dart:convert';

import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:dermatologist/util/appcolors.dart';
import 'package:dermatologist/util/globaldata.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:google_sign_in/google_sign_in.dart';

class Registration extends StatefulWidget {
  Registration({Key key}) : super(key: key);

  @override
  _MyRegistration createState() => _MyRegistration();
}

class _MyRegistration extends State<Registration> {
  bool check = false;
  TextEditingController firstnameController = new TextEditingController();
  TextEditingController lastnameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirm_passwordController =
      new TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _load = false;
  bool _isLoading = false;

  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";
  int _state = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setPrefrenceData();
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();
    HttpHelper.getAdminId();
    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget loadingIndicator = _load
        ? new Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 0.0),
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10.0,
                    color: Colors.black.withOpacity(.3),
                    offset: Offset(6.0, 7.0),
                  ),
                ],
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white),
            width: 70.0,
            height: 70.0,
            child: new Padding(
                padding: const EdgeInsets.all(5.0),
                child: new Center(child: new CircularProgressIndicator())),
          )
        : new Container();

    return Scaffold(
      key: _scaffoldKey,
      body: ListView(children: <Widget>[
        new Container(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    child: new Image.asset(
                      'assets/images/ImageRegistration.jpg',
                      fit: BoxFit.fitWidth,
                    ),
                  ),
//                  Align(
//                    child: loadingIndicator,
//                    alignment: FractionalOffset.center,
//                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(10),
                child: Text(
                  "Dermatologist",
                  style: TextStyle(
                      color: HexColor(topBackColor),
                      fontSize: 40,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 0, horizontal: 35),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 7),
                      child: TextField(
                        controller: firstnameController,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFF81D4FA)),
                              borderRadius: BorderRadius.circular(10)),
                          hintText: "First Name",
                          prefixIcon: Icon(Icons.person_outline),
                          fillColor: Color(0xFFD6D6D6),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextField(
                        controller: lastnameController,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFF81D4FA)),
                              borderRadius: BorderRadius.circular(10)),
                          hintText: "Last Name",
                          prefixIcon: Icon(Icons.person_outline),
                          fillColor: Color(0xFFD6D6D6),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextField(
                        controller: emailController,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFF81D4FA)),
                              borderRadius: BorderRadius.circular(10)),
                          hintText: "Email Address ",
                          prefixIcon: Icon(Icons.mail_outline),
                          fillColor: Color(0xFFD6D6D6),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextField(
                        controller: passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFF81D4FA)),
                              borderRadius: BorderRadius.circular(10)),
                          hintText: "Password",
                          prefixIcon: Icon(Icons.lock_outline),
                          fillColor: Color(0xFFD6D6D6),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: TextField(
                        controller: confirm_passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(color: Colors.grey),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Color(0xFF81D4FA)),
                              borderRadius: BorderRadius.circular(10)),
                          hintText: "Confirm Password",
                          prefixIcon: Icon(Icons.lock_outline),
                          fillColor: Color(0xFFD6D6D6),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 0, horizontal: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Checkbox(
                      value: check,
                      onChanged: (bool value) {
                        setState(() {
                          check = value;
                        });
                      },
                    ),
                    Text(
                      "I agree with terms and Condition",
                      style: TextStyle(fontSize: 13, color: Colors.grey),
                    )
                  ],
                ),
              ),
              Column(
                children: <Widget>[
                  new Container(
                    width: double.infinity,
                    margin: new EdgeInsets.symmetric(horizontal: 30.0),
                    child: ButtonTheme(
                      height: 60,
                      child: RaisedButton(
                        shape: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                                BorderSide(color: HexColor(topBackColor))),
                        child: setUpButtonChild(),
                        onPressed: () {
                          SystemChannels.textInput
                              .invokeMethod('TextInput.hide');
                          checkInternetConnection(context);
                        },
                        color: HexColor(topBackColor),
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 10),
                child: Center(
                  child: Text(
                    "Or",
                    style: TextStyle(color: AppColors.grey),
                  ),
                ),
              ),
              /*Padding(
                padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    ButtonTheme(
                      height: 55,
                      child: RaisedButton(
                        shape: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: Color(0xFFE53935))),
                        child: Image.asset('assets/images/google.jpg'),
                        onPressed: () {
                          //_performGoogleLogin();
                        },
                        color: Color(0xFFE53935),
                      ),
                    ),
                    ButtonTheme(
                      height: 55,
                      child: RaisedButton(
                        shape: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(color: AppColors.blue)),
                        child: Image.asset('assets/images/facebook.jpg'),
                        onPressed: () {},
                        color: AppColors.blue,
                      ),
                    ),
                  ],
                ),
              ),*/
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: new InkWell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Already have an Account? "),
                      Text(
                        "Login",
                        style: TextStyle(
                            color: HexColor(topBackColor),
                            fontWeight: FontWeight.w700),
                      )
                    ],
                  ),
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }

  checkInternetConnection(BuildContext context) async {
    if (await HttpHelper.isConnectedNew(context, checkInternetConnection,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(context, checkInternetConnection,
            barrierDismissible: true)) {
      _performRegister();
    } else {
      buildSnackbar("Please connect to internet");
    }
  }

  buildSnackbar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(
        message,
        style: TextStyle(color: HexColor(topTextColor)),
      ),
      backgroundColor: HexColor(topBackColor),
    ));
  }

  Future _performRegister() async {
    String firstname = firstnameController.text;
    String lastname = lastnameController.text;
    String email = emailController.text;
    String password = passwordController.text;
    String confirm_password = confirm_passwordController.text;

    if (firstname.length == 0) {
      buildSnackbar("Please Enter First Name");
    } else if (lastname.length == 0) {
      buildSnackbar("Please Enter Last Name");
    } else if (email.length == 0) {
      buildSnackbar("Please Enter Email");
    } else if (!GlobalData.isEmailValid(email)) {
      buildSnackbar("Please Enter Valid Email");
    } else if (password.length == 0) {
      buildSnackbar("Please Enter Password");
    } else if (password.length < 4) {
      buildSnackbar("Please set at least 4 character Password");
    } else if (password != confirm_password) {
      buildSnackbar("Password and confirm password does not match");
    } else if (!check) {
      buildSnackbar("Please select Terms and conditions");
    } else {
      setState(() {
        _load = true;
        if (_state == 0) {
          animateButton();
        }
      });

      HttpHelper.doRegistration(
              firstname, lastname, email, password, confirm_password)
          .then((response) {
        print(response.body);

        var parsedJson = json.decode(response.body);
        if (parsedJson['status']) {
          setState(() {
            _load = false;
            _state=0;
          });
          buildSnackbar(parsedJson['message']);
          new Future.delayed(const Duration(seconds: 2), () {
            Navigator.pop(context);
          });
        } else {
          buildSnackbar(parsedJson['message']);
          setState(() {
            _load = false;
            _state=0;
          });
        }
      });
    }
  }

/*Future _performGoogleLogin() async {
    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
    _firebaseMessaging.getToken().then((token) {
      //widget.userData.fcmToken = token;
      print("FCM TOKEN: $token");
    }).catchError((error) {
      print("FCM ERROR: $error");
    });

    print("Perform Google Login");
    if (await HttpHelper.isConnectedNew(context, _performGoogleLogin) != null &&
        !_isLoading) {
      setState(() {
        _isLoading = true;
      });
      GoogleSignIn _googleSignIn = GoogleSignIn(
        scopes: ['email'],
      );
      _googleSignIn.signIn().then((data) {
        print("Email: ${data.email}");
        print("Name: ${data.displayName}");
        print("Image: ${data.photoUrl}");
        */ /*HttpHelper.doLoginCall(data.email, data.displayName, data.photoUrl,
            "google", data.id, widget.userData.fcmToken)
            .then((response) {
          print(response);
          UserModel user =
          UserModel.fromJson(JSON.jsonDecode(response.body)['user']);
          print(user);
          setState(() {
            widget.userData.setUser(user);
            widget.onSignedIn();
            _isLoading = false;
          });
        }).catchError((error) {
          print(error);
          setState(() {
            _isLoading = false;
          });
        });*/ /*
      }).catchError((error) {
        print(error);
        setState(() {
          _isLoading = false;
        });
      });
    }
  }*/

  Widget setUpButtonChild() {
    if (_state == 0) {
      return new Text(
        "Sign Up",
        style: const TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ),
      );
    } else if (_state == 1) {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    }
  }

  void animateButton() {
    setState(() {
      _state = 1;
    });

//    Timer(Duration(milliseconds: 3300), () {
//      setState(() {
//        _state = 2;
//      });
//    });
  }
}
