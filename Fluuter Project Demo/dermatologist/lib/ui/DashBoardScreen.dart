import 'dart:convert';

import 'package:dermatologist/model/UserData.dart';
import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/ui/HomeScreen.dart';
import 'package:dermatologist/ui/MorePage.dart';
import 'package:dermatologist/ui/NewCasePage.dart';
import 'package:dermatologist/util/HexColor.dart';
import 'package:dermatologist/util/HttpHelper.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:flutter/material.dart';

class DashBoardScreen extends StatefulWidget {
  DashBoardScreen(this.userModel, this.userData);

  final UserModel userModel;
  UserData userData;

  @override
  _DashBoardScreen createState() => _DashBoardScreen();
}

class _DashBoardScreen extends State<DashBoardScreen> {
  int _currentIndex = 0;
  BuildContext mcontext;
  SharedPref _sharedPref = new SharedPref();
  String topBackColor = "#FFFFFF",
      topTextColor = "#FFFFFF",
      bottomBackColor = "#FFFFFF",
      bottomTextColor = "#FFFFFF";
  final List<String> header_text = [
    "Dermatologist",
    "New Case",
    "Dermatologist",
  ];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setPrefrenceData();
    _paypalKey();
  }

  _paypalKey() async {
    if (await HttpHelper.isConnectedNew(mcontext, _paypalKey,
                barrierDismissible: true) !=
            null &&
        await HttpHelper.isConnectedNew(mcontext, _paypalKey,
            barrierDismissible: true)) {
      HttpHelper.getPaymentKey().then((response) {
        var jsonParse = json.decode(response.body);
        if (jsonParse['status']) {
          _sharedPref.setPaymentData(
              jsonParse['tokenizationKey'], jsonParse['displayName']);
//          sandboxKey = jsonParse['tokenizationKey'];
//          sandbox_companyName = jsonParse['displayName'];
        }
      }).catchError((error) {});
    }
  }

  void setPrefrenceData() {
    _sharedPref = new SharedPref();

    _sharedPref.getTopBackColor().then((color) {
      setState(() {
        topBackColor = color;
      });
    });
    _sharedPref.getBottomBackColor().then((color) {
      setState(() {
        bottomBackColor = color;
      });
    });
    _sharedPref.getBottomTextColor().then((color) {
      setState(() {
        bottomTextColor = color;
      });
    });

    _sharedPref.getTopTextColor().then((color) {
      setState(() {
        topTextColor = color;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> _children = [
      HomeScreen(widget.userModel, widget.userData),
      NewCasePage(false, widget.userModel, widget.userData),
//      Successfull_caseadded()
      MorePage(widget.userData, widget.userModel),
    ];
    return new WillPopScope(
        child: new Scaffold(
            appBar: AppBar(
                centerTitle: true,
                backgroundColor: HexColor(topBackColor),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    new Text(
                      header_text[_currentIndex],
                      style: TextStyle(
                          color: HexColor(topTextColor), fontSize: 25),
                    )
                  ],
                )),
            body: _children[_currentIndex],
            bottomNavigationBar: new Theme(
              data: ThemeData(canvasColor: HexColor(bottomBackColor)),
              child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                currentIndex: _currentIndex,
                selectedFontSize: 15,
                unselectedFontSize: 12,
                selectedLabelStyle: TextStyle(color: HexColor(topBackColor)),
                backgroundColor: HexColor(bottomBackColor),
                selectedItemColor: HexColor(topBackColor),
                items: [
                  BottomNavigationBarItem(
                    icon: new Icon(
                      Icons.home,
                      color: HexColor(bottomTextColor),
                    ),
                    title: new Text(
                      'Home',
                      style: TextStyle(
                        color: HexColor(bottomTextColor),
                      ),
                    ),
                  ),
                  BottomNavigationBarItem(
                    icon: new Icon(
                      Icons.playlist_add,
                      color: HexColor(bottomTextColor),
                    ),
                    title: new Text('New case',
                        style: TextStyle(
                          color: HexColor(bottomTextColor),
                        )),
                  ),
                  BottomNavigationBarItem(
                      icon: Icon(
                        Icons.more_vert,
                        color: HexColor(bottomTextColor),
                      ),
                      title: Text('More',
                          style: TextStyle(
                            color: HexColor(bottomTextColor),
                          )))
                ],
                onTap: onTabTapped, // new
              ),
            )),
        onWillPop: () async {
          return showDialog(
                context: context,
                builder: (context) => new AlertDialog(
                  title: new Text('Are you sure?'),
                  content: new Text('Do you want to exit an App'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(false),
                      child: new Text('No'),
                    ),
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(true),
                      child: new Text('Yes'),
                    ),
                  ],
                ),
              ) ??
              false;
        });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
