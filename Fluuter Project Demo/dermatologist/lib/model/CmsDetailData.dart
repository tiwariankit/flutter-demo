class CMSData {
  String id, title,content;
  CMSData(this.id,this.title,this.content);

  CMSData.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        title = json['title'],

        content = json['content'];


  Map<String, dynamic> toJson() => {
    'id': id,
    'title': title,

    'content': content,

  };
}