class PrescriptionData {
  String prescription, firstname, lastname;

  PrescriptionData(this.prescription,this.firstname,this.lastname);

  PrescriptionData.fromJson(Map<String, dynamic> json)
      : prescription = json["prescription"],
        firstname = json['firstname'],
        lastname = json['lastname'];

  Map<String, dynamic> toJson() => {
        'prescription': prescription,
        'firstname': firstname,
        'lastname': lastname,
      };
}
