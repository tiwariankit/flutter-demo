class DoctorDetailsData {
  String  firstname,lastname,email,image,birthdate,gender,description;

  DoctorDetailsData();

  DoctorDetailsData.fromJson(Map<String, dynamic> json)
      :firstname = json['firstname'],
        lastname = json['lastname'],
        image=json['image'],
        birthdate=json['birthdate'],
        gender=json['gender'],
        description=json['description'],
        email = json['email'];


  Map<String, dynamic> toJson() => {
    'firstname': firstname,
    'lastname': lastname,
    'email': email,
    'birthdate':birthdate,
    'gender':gender,
    'image':image,
    'description':description,
  };
}
