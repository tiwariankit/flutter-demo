class ClinicData {
  String id, name,location,latitute,longitute,website,phone,specialities,distance;


  ClinicData(this.id,this.name,this.location,this.latitute,this.longitute,this.website,this.phone,this.specialities,this.distance);

  ClinicData.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        name = json['name'],
        location = json['location'],
        latitute = json['latitute'],
        longitute = json['longitute'],
        website = json['website'],
        phone=json['phone'],
        specialities=json['specialities'],
        distance=json['distance'];


  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'location': location,
    'latitute': latitute,
    'longitute': longitute,
    'website': website,
    'phone': phone,
    'specialities':specialities,
    'distance':distance,
  };
}
