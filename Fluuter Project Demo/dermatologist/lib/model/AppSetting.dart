class AppSetting {
  String id,
      admin_id,
      logo,
      name,
      color,
      footer_color,
      header_text_color,
      footer_text_color,
      address,
      latitute,
      longitute;

  AppSetting.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        admin_id = json['admin_id'],
        logo = json['logo'],
        name = json['name'],
        color = json['color'],
        footer_color = json['footer_color'],
        header_text_color = json['header_text_color'],
        footer_text_color = json['footer_text_color'],
        address = json['address'],
        latitute = json['latitute'],
        longitute = json['longitute'];

  AppSetting(
      this.id,
      this.admin_id,
      this.logo,
      this.name,
      this.color,
      this.footer_color,
      this.header_text_color,
      this.footer_text_color,
      this.address,
      this.latitute,
      this.longitute);

  Map<String, dynamic> toJson() => {
        'id': id,
        'admin_id': admin_id,
        'logo': logo,
        'name': name,
        'color': color,
        'footer_color': footer_color,
        'header_text_color': header_text_color,
        'footer_text_color': footer_text_color,
        'address': address,
        'latitute': latitute,
        'longitute': longitute,
      };
}
