class CmsListData {
  String id, title, admin_id;

  CmsListData(this.id, this.title, this.admin_id);

  CmsListData.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        title = json['title'],
        admin_id = json['admin_id'];

  Map<String, dynamic> toJson() =>
      {'id': id, 'title': title, 'admin': admin_id};
}

