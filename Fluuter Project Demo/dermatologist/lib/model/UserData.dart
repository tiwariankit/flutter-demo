import 'dart:convert' as JSON;

import 'package:dermatologist/model/UserModel.dart';
import 'package:dermatologist/util/SharedPref.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class UserData {
  bool loggedIn = false;
  String fcmToken;

  Future<bool> isLoggedIn();

  void setUser(UserModel user) {}

  UserModel getUser();

  void signOut() {}
}

class UserDataImp implements UserData {
  @override
  bool loggedIn;

  SharedPreferences prefs;
  UserModel user = null;

  @override
  UserModel getUser() {
    // TODO: implement getUser
    if (user == null) {
      user = UserModel.fromJson(JSON.jsonDecode(prefs.getString("user")));
      return user;
    } else
      return user;
  }

  @override
  Future<bool> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    this.prefs = prefs;
    if (prefs.getBool("isLogin") == null) {
      loggedIn = false;
      return false;
    } else if (prefs.getBool("isLogin")) {
      getUser();
      loggedIn = true;
      return true;
    } else {
      loggedIn = false;
      return false;
    }
    // TODO: implement isLoggedIn
    return null;
  }

  @override
  void setUser(UserModel user) {
    prefs.setBool("isLogin", true);
    prefs.setString("user", JSON.jsonEncode(user));

    SharedPref sharedPref = new SharedPref();
    sharedPref.setUserData(user);
    prefs.commit();
    this.user = user;

    // TODO: implement setUser
  }

  @override
  void signOut() {
    prefs.setBool("isLogin", false);
    // TODO: implement signOut
  }

  @override
  String fcmToken;
}
