class DoctorListData {
  String  id,firstname,lastname,email,image;


  //DoctorListData(this.firstname,  this.lastname, this.email,this.image);
  DoctorListData();

  DoctorListData.fromJson(Map<String, dynamic> json)
      : id=json['id'],
        firstname = json['firstname'],
        lastname = json['lastname'],
        image=json['image'],
        email = json['email'];


  Map<String, dynamic> toJson() => {

    'firstname': firstname,
    'lastname': lastname,
    'email': email,
    'image':image,
  };
}
