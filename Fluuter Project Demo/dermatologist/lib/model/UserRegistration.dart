class UserRegistration{
  String firstname,lastname,email,password,confirm_password;


  UserRegistration(this.firstname, this.lastname, this.email, this.password,
      this.confirm_password);


  UserRegistration.fromJson(Map<String, dynamic> json)
      :firstname = json['firstname'],
        lastname=json['lastname'],
        email = json['email'],
        password = json['password'],
        confirm_password=json['reward_point']
  ;

  Map<String, dynamic> toJson() => {
    'firstname': firstname,
    'lastname':lastname,
    'email': email,
    'password': password,
    'confirm_password': confirm_password,

  };
}