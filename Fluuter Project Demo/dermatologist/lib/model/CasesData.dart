class CasesData {
  String appId,
      id,
      Name,
      sex,
      user_id,
      contact,
      description,
      date,
      case_id,
      time,
      problem_occur,
      transaction_id,
      amount,
      is_offline;
  List<dynamic> photos;

  CasesData(
      this.appId,
      this.id,
      this.Name,
      this.photos,
      this.sex,
      this.user_id,
      this.contact,
      this.description,
      this.date,
      this.case_id,
      this.time,
      this.problem_occur,
      this.transaction_id,
      this.amount,
      this.is_offline);

  CasesData.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        Name = json['Name'],
        sex = json['sex'],
        user_id = json['user_id'],
        contact = json['contact'],
        description = json['description'],
        date = json['date'],
        case_id = json['case_id'],
        time = json['time'],
        photos = json['photos'],
        problem_occur = json['problem_occur'],
        amount = json['amount'],
        is_offline = json['is_offline'],
        transaction_id = json['transaction_id'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'Name': Name,
        'sex': sex,
        'user_id': user_id,
        'contact': contact,
        'description': description,
        'photos': photos,
        'date': date,
        'time': time,
        'case_id': case_id,
        'problem_occur': problem_occur,
        'transaction_id': transaction_id,
        'is_offline': is_offline,
        'amount': amount
      };
}
