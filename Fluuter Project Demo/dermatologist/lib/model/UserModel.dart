
class UserModel{
  String user_id,firstname,lastname,email,image;

  UserModel(this.user_id, this.firstname, this.lastname, this.email,this.image);
  UserModel.fromJson(Map<String, dynamic> json)
      : user_id = json["user_id"],
        firstname = json['firstname'],
        lastname = json['lastname'],
        email = json['email'],
        image = json['image'];

  Map<String, dynamic> toJson() => {
    'user_id': user_id,
    'firstname': firstname,
    'lastname': lastname,
    'email': email,
    'image': image,
  };
}