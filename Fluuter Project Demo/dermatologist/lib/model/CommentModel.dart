class CommentModel {
  String id,
      case_id,
      images,
      description,
      profile_image,
      sender_name,
      message_date,
      message_time;

  CommentModel(
      this.id,
      this.case_id,
      this.images,
      this.description,
      this.profile_image,
      this.sender_name,
      this.message_date,
      this.message_time);

  CommentModel.fromJson(Map<String, dynamic> json)
      : id = json["id"],
        case_id = json['case_id'],
        images = json['images'],
        description = json['description'],
        profile_image = json['profile_image'],
        sender_name = json['sender_name'],
        message_date = json['message_date'],
        message_time = json['message_time'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'case_id': case_id,
        'images': images,
        'description': description,
        'profile_image': profile_image,
        'sender_name': sender_name,
        'message_date': message_date,
        'message_time': message_time
      };
}
