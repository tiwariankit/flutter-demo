import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyCalc());

class MyCalc extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Calculator",
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.deepOrange,
      ),
      home: Calculator(),
    );
  }
}
//ddd
class Calculator extends StatefulWidget {
  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {
//  var num1 = TextEditingController();
//  var num2 = TextEditingController();

  TextEditingController num1 = TextEditingController();
  TextEditingController num2 = TextEditingController();

  var n1 = 0, n2 = 0, ans = 0;

  void doAddition() {
    setState(() {
      n1 = int.parse(num1.text);
      n2 = int.parse(num2.text);
      ans = n1 + n2;
    });
  }

  void doSub() {
    setState(() {
      n1 = int.parse(num1.text);
      n2 = int.parse(num2.text);
      ans = n1 - n2;
    });
  }

  void doMul() {
    setState(() {
      n1 = int.parse(num1.text);
      n2 = int.parse(num2.text);
      ans = n1 * n2;
    });
  }

  void doDiv() {
    setState(() {
      n1 = int.parse(num1.text);
      n2 = int.parse(num2.text);
      ans = n1 ~/ n2;
    });
  }

  void doClear() {
    setState(() {
      num1.text = "0";
      num2.text = "0";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffadd8e6),
      appBar: AppBar(
        title: Text("Calculator"),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).canPop()),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "OutPut is:$ans",
              style: TextStyle(
                  color: Colors.lightBlue,
                  fontSize: 40.0,
                  fontWeight: FontWeight.w700),
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: num1,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Enter No1:",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  borderSide: BorderSide(color: Colors.white),
                ),
                prefixIcon: Icon(Icons.confirmation_number),
                filled: true,
                fillColor: Colors.white,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: num2,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                hintText: "Enter No2:",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  borderSide: BorderSide(color: Colors.white),
                ),
                prefixIcon: Icon(Icons.confirmation_number),
                filled: true,
                fillColor: Colors.white,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: RaisedButton(
                    onPressed: doAddition,
                    color: Colors.deepOrangeAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Icon(Icons.add),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    onPressed: doSub,
                    color: Colors.deepOrangeAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Icon(Icons.remove),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: RaisedButton(
                    onPressed: doDiv,
                    color: Colors.deepOrangeAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Text("/",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                        )),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    onPressed: doMul,
                    color: Colors.deepOrangeAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Icon(Icons.close),
                  ),
                ),
              ],
            ),
            RaisedButton(
              child: Text("Clear"),
              onPressed: doClear,
              color: Colors.deepOrangeAccent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
