import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(AppBarProfile());

class AppBarProfile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Profile Page",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          brightness: Brightness.light, primarySwatch: Colors.blueGrey),
      home: ProfilePage(),
    );
  }
}

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var btncolor = Colors.deepOrange;
  bool logoutButtonVisible = true;
  bool updateButtonVisible = false;
  bool btndisable = true;
  bool textFieldEnabled = false;
  bool editIconImage = false;

  void _showDailog(){
    showDialog(
        context: context,
        builder: (BuildContext context){
          return SingleChildScrollView(
            child: Center(
              child: AlertDialog(
                title: Text("Logout"),
                content: Text("Are you sure you want to logout"),
                actions: <Widget>[
                  FlatButton(
                      onPressed:()=> Navigator.of(context).pop(),
                      child: Text("Cancel",style: TextStyle(color: Colors.deepOrange),)
                  ),
                  FlatButton(
                      onPressed:()=> Navigator.of(context).pop(),
                      child: Text("Ok",style: TextStyle(color: Colors.deepOrange),)
                  ),
                ],
              ),
            ),
          );
      }
    );
  }

  void changeColor() {
    setState(() {
      if (logoutButtonVisible == true) {
        updateButtonVisible = true;
        logoutButtonVisible = false;
        textFieldEnabled = true;
        editIconImage = true;
        btndisable = false;
      } else {
        logoutButtonVisible = true;
        updateButtonVisible = false;
        textFieldEnabled = false;
        editIconImage = false;
        btndisable = true;
      }
    });
  }

  Widget logoutButton() {
    return Align(
      alignment: FractionalOffset.bottomCenter,
      child: Container(
        width: double.infinity,
        height: 30.0,
        child: Visibility(
          visible: logoutButtonVisible,
          child: RaisedButton(
            onPressed:  _showDailog,
            child: Text("Logout", style: TextStyle(color: Colors.white)),
            color: Colors.deepOrange,
          ),
        ),
      ),
    );
  }

  Widget updateButton() {
    return Align(
      alignment: FractionalOffset.bottomCenter,
      child: Container(
        width: double.infinity,
        height: 30.0,
        child: Visibility(
          visible: updateButtonVisible,
          child: RaisedButton(
            onPressed: () => print("Update Button"),
            child:
            Text("Update Profile", style: TextStyle(color: Colors.white)),
            color: Colors.blueGrey,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomAppBar(
        color: Colors.yellow,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Visibility(
              visible: logoutButtonVisible,
              child: logoutButton(),
            ),
            Visibility(
              visible: updateButtonVisible,
              child: updateButton(),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text("Profile"),
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.of(context).pop()),
        centerTitle: true,
        actions: <Widget>[
          Visibility(
            visible: btndisable,
            child: IconButton(
              icon: Icon(Icons.edit),
              onPressed: changeColor,
            ),
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 150.0,
                color: Colors.blueGrey,
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 100.0),
                  height: 100.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.green,
                    image: DecorationImage(
                      fit: BoxFit.fill,
                      image: NetworkImage(
                        "https://www.colourbox.com/preview/37323141-portrait-man-cartoon-business-employee-character.jpg",
                      ),
                    ),
                  ),
                  //margin: EdgeInsets.only(top: 97.0),
                ),
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 150.0, left: 95.0),
                  child: Visibility(
                    visible: editIconImage,
                    child: CircleAvatar(
                        backgroundColor: Colors.deepOrange,
                        radius: 13.0,
                        child: Icon(
                          Icons.edit,
                          size: 17.0,
                        )),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 200.0),
                padding: EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(child: Text("Ankit Tiwari")),
                    SizedBox(
                      height: 15.0,
                    ),
                    Text("Email:"),
                    Divider(
                      //color: Colors.blueGrey,
                      thickness: 2.0,
                    ),
                    SizedBox(height: 10.0),
                    Container(
                      height: 20.0,
                      child: TextField(
                        keyboardType: TextInputType.emailAddress,
                        style: TextStyle(
                          color: Colors.blueGrey,
                          fontSize: 14.0,
                          fontWeight: FontWeight.w600,
                        ),
                        decoration: InputDecoration(
                            hintText: "Phone",
                            enabled: textFieldEnabled,
                            enabledBorder: UnderlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(0)),
                                borderSide: BorderSide(color: Colors.grey)),
                            alignLabelWithHint: false),
                      ),
                    ),
//                     Divider(
//                       color: Colors.blueGrey,
//                       thickness: 2.0,
//                     ),
                    SizedBox(height: 10.0),
                    Text("Designation:"),
                    Divider(
                      //color: Colors.blueGrey,
                      thickness: 2.0,
                    ),
                    SizedBox(height: 10.0),
                    Text("Department:"),
                    Divider(
                      //color: Colors.blueGrey,
                      thickness: 2.0,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
