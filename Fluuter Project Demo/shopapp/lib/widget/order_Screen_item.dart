import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shopapp/provider/order.dart';

class OrderScreenItem extends StatefulWidget {
  Order order;

  OrderScreenItem({this.order});

  @override
  _OrderScreenItemState createState() => _OrderScreenItemState();
}

class _OrderScreenItemState extends State<OrderScreenItem> {
  var isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(15.0),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(widget.order.amount.toString()),
            subtitle:
                Text(DateFormat('dd MM yyyy hh:mm').format(widget.order.dateTime)),
            trailing: IconButton(
                icon: Icon(isExpanded ? Icons.expand_less : Icons.expand_more),
                onPressed: () {
                  setState(() {
                    isExpanded = !isExpanded;
                  });
                }),
          ),
          if (isExpanded)
            Container(
              //padding: EdgeInsets.symmetric(horizontal: 15.0,vertical: 15.0),
                height: 100.0,
                child: Column(
                  children:widget.order.products.map((prod)=> Row(
                    children: <Widget>[
                      Text(prod.title),
                      Text('${prod.quantity}*\$${prod.price}'),
                    ],
                  )).toList(),
                ))
        ],
      ),
    );
  }
}
