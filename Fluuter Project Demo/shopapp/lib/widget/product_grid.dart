import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/provider/products.dart';
import 'product_item.dart';

class ProductGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final productData = Provider.of<Products>(context);
    final loadedProduct = productData.loadedProduct;
    return GridView.builder(
      padding: EdgeInsets.all(10.0),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3 / 2,
          mainAxisSpacing: 10.0,
          crossAxisSpacing: 10.0),
      itemBuilder: (context, index) {
        return ChangeNotifierProvider.value(
          value:loadedProduct[index],
          child: ProductItem(
//            id: loadedProduct[index].id,
//            title: loadedProduct[index].title,
//            imageUrl: loadedProduct[index].imageUrl,
          ),
        );
      },
      itemCount: loadedProduct.length,
    );
  }
}
