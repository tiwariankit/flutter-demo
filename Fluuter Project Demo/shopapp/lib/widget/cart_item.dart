import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/provider/cart.dart';

class CartItems extends StatelessWidget {
  final double price;
  final String productId;
  final String title;
  final int qty;
  final String id;

  CartItems({this.price, this.title, this.id, this.qty, this.productId});

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      confirmDismiss: (direction) {
        return showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text("Are you sure?",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),),
                  content: Text("Do you want to delete"),
                  actions: <Widget>[
                    FlatButton(
                        onPressed: () => Navigator.of(ctx).pop(false),
                        child: Text("No")),
                    FlatButton(
                        onPressed: () => Navigator.of(ctx).pop(true),
                        child: Text("Yes"))
                  ],
                ));
      },
      key: ValueKey(id),
      onDismissed: (direction) {
        Provider.of<CartItem>(context, listen: false).removeItem(productId);
      },
      direction: DismissDirection.endToStart,
      background: Container(
        color: Colors.red,
        padding: EdgeInsets.only(right: 20.0),
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 50.0,
        ),
      ),
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: ListTile(
            leading: CircleAvatar(
              radius: 25.0,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  price.toString(),
                  style: TextStyle(fontSize: 12.0),
                ),
              ),
            ),
            title: Text(title),
            subtitle: Text('Total \$${price * qty}'),
            trailing: Text("$qty x"),
          ),
        ),
      ),
    );
  }
}
