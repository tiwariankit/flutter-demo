import 'package:flutter/material.dart';

class Product extends ChangeNotifier{
  String id;
  String title;
  String discription;
  double price;
  String imageUrl;
  bool isFavorite;

  Product({this.id, this.title, this.discription, this.price, this.imageUrl,this.isFavorite=false});

  void toggleChngeButton(){
    isFavorite=!isFavorite;
    notifyListeners();
  }
}