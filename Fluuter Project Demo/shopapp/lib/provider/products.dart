import 'package:flutter/material.dart';
import 'package:shopapp/model/product.dart';

class Products extends ChangeNotifier {
  List<Product> _loadedProduct = [
    Product(
      id: 'p1',
      title: 'Red Shirt',
      discription: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
          'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    ),
    Product(
      id: 'p2',
      title: 'Trousers',
      discription: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
    ),
    Product(
      id: 'p3',
      title: 'Yellow Scarf',
      discription: 'Warm and cozy - exactly what you need for the winter.',
      price: 19.99,
      imageUrl:
          'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    ),
    Product(
      id: 'p4',
      title: 'A Pan',
      discription: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
    ),
  ];
  var _showFavoriteOnly = false;

  List<Product> get loadedProduct {
    if (_showFavoriteOnly) {
      return _loadedProduct.where((prod) => prod.isFavorite).toList();
    }
    return _loadedProduct;
  }

  void shoFavoriteOnly() {
    _showFavoriteOnly = true;
    notifyListeners();
  }

  void showAll() {
    _showFavoriteOnly = false;
    notifyListeners();
  }

  Product findById(String id) {
    return _loadedProduct.firstWhere((prod) => prod.id == id);
  }

  String get count => _loadedProduct.length.toString();

  void add(Product product) {
    _loadedProduct.add(Product(
        id: DateTime.now().toString(),
        price: product.price,
        discription: product.discription,
        imageUrl: product.imageUrl,
        title: product.title));
    notifyListeners();
  }

  void editProduct(String id, Product product) {
    final prodIndex = _loadedProduct.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      _loadedProduct[prodIndex] = product;
    } else {
      print("------");
    }
  }

  void removeProduct(String productId) {
    _loadedProduct.removeWhere((prod) {
      return prod.id == productId;
    });
    notifyListeners();
  }
}
