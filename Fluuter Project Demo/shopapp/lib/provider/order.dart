import 'package:flutter/material.dart';
import 'package:shopapp/provider/cart.dart';
import 'package:shopapp/widget/cart_item.dart';

class Order {
  String id;
  double amount;
  List<Cart> products;
  DateTime dateTime;

  Order({this.id, this.amount, this.products,this.dateTime});
}

class OrderItem extends ChangeNotifier {
  List<Order> _orders=[];

  List<Order> get orders {
    return _orders;
  }

  void addOrder(List<Cart> cartProducts, double total) {
    _orders.insert(0,
        Order(
            id: DateTime.now().toString(),
            amount: total,
            dateTime: DateTime.now(),
            products: cartProducts));
    notifyListeners();
  }
}
