import 'package:flutter/foundation.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Cart {
  String id;
  String title;
  double price;
  int quantity;

  Cart({this.id, this.title, this.price, this.quantity});
}

class CartItem extends ChangeNotifier {
  Map<String, Cart> _items = {};

  Map<String, Cart> get items => {..._items};

  int get itemCount {
    return _items.length == null ? 0 : _items.length;
  }

  void removeItem(String id) {
    _items.remove(id);
    notifyListeners();
  }

  void clearCart() {
    _items = {};
    notifyListeners();
  }

  double get totalAmount {
    double amount = 0.0;
    _items.forEach((key, total) {
      amount += total.price * total.quantity;
    });
    return amount;
  }

  void add(String productId, String title, double price) {
    if (_items.containsKey(productId)) {
      _items.update(
          productId,
              (existingItem) =>
              Cart(
                  id: existingItem.id,
                  title: existingItem.title,
                  price: existingItem.price,
                  quantity: existingItem.quantity + 1));
    } else {
      _items.putIfAbsent(
          productId,
              () =>
              Cart(
                  id: DateTime.now().toString(),
                  title: title,
                  price: price,
                  quantity: 1));
    }
    notifyListeners();
  }
  void removeItemFromCart(String productId) {
    if (_items.containsKey(productId) == 0) {
      return;
    }
    if (_items[productId].quantity > 1) {
      _items.update(productId, (existingItem) =>
          Cart(id: existingItem.id,
              quantity: existingItem.quantity - 1,
              price: existingItem.price,
              title: existingItem.title));
    }else{
      _items.remove(productId);
    }
    notifyListeners();
  }
}
