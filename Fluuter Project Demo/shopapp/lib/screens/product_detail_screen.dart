import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../provider/products.dart';
class ProductDetailScreen extends StatelessWidget {

  static const routeName='/product-detail';
  @override
  Widget build(BuildContext context) {
    final productId=ModalRoute.of(context).settings.arguments as String;
    final loadedProdectItem=Provider.of<Products>(context).findById(productId);
    return Scaffold(
      appBar: AppBar(title: Text(loadedProdectItem.title),),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(loadedProdectItem.imageUrl),
            ),
            SizedBox(height: 10,),
            Text('\$${loadedProdectItem.price}'),
            SizedBox(height: 10,),
            Text("${loadedProdectItem.discription}",style: TextStyle(fontSize: 22.0,fontWeight: FontWeight.bold),),
          ],
        ),
      ),
    );
  }
}
