import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/provider/products.dart';
import 'package:shopapp/screens/edit_product_screen.dart';
import 'package:shopapp/widget/app_drawer.dart';
import 'package:shopapp/widget/user_product_item.dart';

class UserProduuctScreen extends StatelessWidget {
  static const routeName = "/user-product";

  @override
  Widget build(BuildContext context) {
    final productData = Provider.of<Products>(context);
    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
        title: Text("Your Product"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).pushNamed(EditProductScreen.routeName);
              }),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView.builder(
            itemCount: productData.loadedProduct.length,
            itemBuilder: (_, index) => UserProductItem(
                  title: productData.loadedProduct[index].title,
                  imageUrl: productData.loadedProduct[index].imageUrl,
                  id: productData.loadedProduct[index].id,
                )),
      ),
    );
  }
}
