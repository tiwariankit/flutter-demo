import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/provider/cart.dart';
import 'package:shopapp/provider/products.dart';
import 'package:shopapp/screens/cart_screen.dart';
import 'package:shopapp/widget/app_drawer.dart';
import 'package:shopapp/widget/product_grid.dart';

enum FilterOption { Favorite, All }

class ProductOverviewScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Products>(context,listen: true);
    final cartItem=Provider.of<CartItem>(context);
    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
        actions: <Widget>[
          PopupMenuButton(
              onSelected: (FilterOption selectedValue) {
                if (selectedValue == FilterOption.Favorite) {
                  product.shoFavoriteOnly();
                } else {
                  product.showAll();
                }
              },
              icon: Icon(Icons.more_vert),
              itemBuilder: (context) => [
                    PopupMenuItem(
                      child: Text("Only Faviourite"),
                      value: FilterOption.Favorite,
                    ),
                    PopupMenuItem(
                      child: Text("Show All"),
                      value: FilterOption.All,
                    )
                  ]),
          InkWell(
            onTap: ()=>Navigator.of(context).pushNamed(CartScreen.routename),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 17.0, right: 5.0),
                  child: Icon(Icons.shopping_cart),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: CircleAvatar(
                      backgroundColor: Theme.of(context).accentColor,
                      radius: 8.0,
                      child: Text(cartItem.itemCount.toString(),style: TextStyle(fontSize: 8.0,fontWeight: FontWeight.bold),)),
                ),
              ],
            ),
          ),
        ],
        title: Text("MyShop"),
      ),
      body: ProductGrid(),
    );
  }
}
