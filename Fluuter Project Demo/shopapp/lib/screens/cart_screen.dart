import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/provider/cart.dart';
import 'package:shopapp/provider/order.dart';
import 'package:shopapp/widget/cart_item.dart';

class CartScreen extends StatelessWidget {
  static const routename = 'cart-screen';

  @override
  Widget build(BuildContext context) {
    final cartDetails = Provider.of<CartItem>(context, listen: true);
    return Scaffold(
      appBar: AppBar(title: Text("Cart Details")),
      body: Column(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(15.0),
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Total",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 22.0),
                  ),
                  Spacer(),
                  Chip(
                    label: Text(
                      "\$${double.parse(cartDetails.totalAmount.toString()).toStringAsFixed(2)}",
                      style: TextStyle(
                          color: Theme.of(context).textTheme.title.color),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  FlatButton(
                    onPressed: (){
                      Provider.of<OrderItem>(context,listen: false).addOrder(cartDetails.items.values.toList(), cartDetails.totalAmount);
                      cartDetails.clearCart();
                    },
                    child: Text(
                      "Order Now",
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 15.0,
          ),
          Expanded(
            child: ListView.builder(
              itemCount: cartDetails.itemCount,
              itemBuilder: (context, index) {
                return CartItems(
                  price: cartDetails.items.values.toList()[index].price,
                  productId: cartDetails.items.keys.toList()[index],
                  title: cartDetails.items.values.toList()[index].title,
                  id: cartDetails.items.values.toList()[index].id,
                  qty: cartDetails.items.values.toList()[index].quantity,
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
