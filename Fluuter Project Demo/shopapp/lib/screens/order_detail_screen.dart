import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/provider/order.dart';
import 'package:shopapp/widget/app_drawer.dart';
import 'package:shopapp/widget/order_Screen_item.dart';

class OrderScreen extends StatefulWidget {
  static const routeName='order=detail';

  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  @override
  Widget build(BuildContext context) {
    final orderData = Provider.of<OrderItem>(context);
    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
        title: Text("Your Order"),
      ),
      body: ListView.builder(
          itemCount: orderData.orders.length,
          itemBuilder: (context, index) =>
              OrderScreenItem(order: orderData.orders[index],)),
    );
  }
}
