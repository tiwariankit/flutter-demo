import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shopapp/model/product.dart';
import 'package:shopapp/provider/products.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = "/edit-product";

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final priceFocusNode = FocusNode();
  final descFocusNode = FocusNode();
  final imageUrlController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  var editedProduct =
      Product(id: null, title: '', price: 0, discription: '', imageUrl: '');

  @override
  void initState() {
    // TODO: implement initState
    setState(() {});
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    priceFocusNode.dispose();
    descFocusNode.dispose();
    imageUrlController.dispose();
    super.dispose();
  }

  void saveForm() {
    final validate = formKey.currentState.validate();
    if (validate == false) {
      return;
    }
    formKey.currentState.save();
    if (editedProduct.id != null) {
      Provider.of<Products>(context, listen: false)
          .editProduct(editedProduct.id, editedProduct);
    } else {
      Provider.of<Products>(context, listen: false).add(editedProduct);
    }
    Navigator.of(context).pop();
  }

  var initValue = {
    'title': '',
    'desc': '',
    'imageUrl': '',
    'price': '',
  };
  var isInit = false;

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    if (isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        final product = Provider.of<Products>(context,listen: false).findById(productId);
        initValue = {
          'title': product.title,
          'desc': product.discription,
          'imageUrl': '',
          'price': product.price.toString(),
        };
        imageUrlController.text = editedProduct.imageUrl;
      }
    }
    isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(icon: Icon(Icons.save), onPressed: saveForm)
        ],
        title: Text("Edit Product"),
      ),
      body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
              key: formKey,
              child: ListView(
                children: <Widget>[
                  TextFormField(
                    initialValue: initValue['title'],
                    onSaved: (value) {
                      editedProduct = Product(
                          id: editedProduct.id,
                          isFavorite: editedProduct.isFavorite,
                          title: value,
                          imageUrl: editedProduct.imageUrl,
                          discription: editedProduct.discription,
                          price: editedProduct.price);
                    },
                    decoration: InputDecoration(
                      labelText: "Title",
                    ),
                    textInputAction: TextInputAction.next,
                    onFieldSubmitted: (_) {
                      FocusScope.of(context).requestFocus(priceFocusNode);
                    },
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter title';
                      } else {
                        return null;
                      }
                    },
                  ),
                  TextFormField(
                      initialValue: initValue['price'],
                      onSaved: (value) {
                        editedProduct = Product(
                            id: editedProduct.id,
                            isFavorite: editedProduct.isFavorite,
                            title: editedProduct.title,
                            imageUrl: editedProduct.imageUrl,
                            discription: editedProduct.discription,
                            price: double.parse(value));
                      },
                      focusNode: priceFocusNode,
                      decoration: InputDecoration(
                        labelText: "Price",
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'enter price';
                        }
                        if (double.tryParse(value) == null) {
                          return 'please enter valid no';
                        }
                        if (double.parse(value) <= 0) {
                          return 'please eneter above zero';
                        }
                        return null;
                      },
                      textInputAction: TextInputAction.next,
                      keyboardType: TextInputType.number,
                      onFieldSubmitted: (_) {
                        FocusScope.of(context).requestFocus(descFocusNode);
                      }),
                  TextFormField(
                      initialValue: initValue['desc'],
                      onSaved: (value) {
                        editedProduct = Product(
                            id: editedProduct.id,
                            isFavorite: editedProduct.isFavorite,
                            title: editedProduct.title,
                            imageUrl: editedProduct.imageUrl,
                            discription: value,
                            price: editedProduct.price);
                      },
                      focusNode: descFocusNode,
                      decoration: InputDecoration(labelText: "Description"),
                      textInputAction: TextInputAction.next,
                      maxLines: 3,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'enter discription';
                        }
                        if (value.length < 10) {
                          return 'Please enter valid discription';
                        }
                      }),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        height: 100,
                        width: 100,
                        margin: const EdgeInsets.only(top: 10, right: 10),
                        decoration: BoxDecoration(
                            border: Border.all(width: 1, color: Colors.grey)),
                        child: imageUrlController.text.isEmpty
                            ? Text("No url")
                            : FittedBox(
                                fit: BoxFit.cover,
                                child: Image.network(imageUrlController.text),
                              ),
                      ),
                      Expanded(
                          child: TextFormField(
                        onSaved: (value) {
                          editedProduct = Product(
                              id: editedProduct.id,
                              isFavorite: editedProduct.isFavorite,
                              title: editedProduct.title,
                              imageUrl: value,
                              discription: editedProduct.discription,
                              price: editedProduct.price);
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'enter image url';
                          }
                          if (!value.startsWith("https") ||
                              !value.startsWith("http")) {
                            return 'please enter valid url';
                          }
                          if (!value.endsWith(".png") &&
                              !value.endsWith(".jpg") &&
                              value.endsWith(".jpeg")) {
                            return 'please enter valid url';
                          }
                          return null;
                        },
                        decoration: InputDecoration(labelText: "Image url"),
                        controller: imageUrlController,
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.url,
                        onFieldSubmitted: (_) {
                          saveForm();
                        },
                      )),
                    ],
                  )
                ],
              ))),
    );
  }
}
