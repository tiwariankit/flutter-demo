import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import './dummy_data.dart';

class MealDetail extends StatelessWidget {
  final String id;

  MealDetail(this.id);

  @override
  Widget build(BuildContext context) {
    final mealDetail = DUMMY_MEALS.firstWhere((meal) => meal.id == id);

    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.delete,
        ),
        onPressed: () => Navigator.of(context).pop(id),
      ),
      appBar: AppBar(
        title: Text(mealDetail.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 250,
              width: double.infinity,
              child: Image.network(
                mealDetail.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: Text(
                "Ingrediants",
                style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              width: 300.0,
              height: 150.0,
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  color: Colors.white, border: Border.all(color: Colors.grey)),
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return Card(
                    color: Colors.amberAccent,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5.0, horizontal: 10.0),
                      child: Text(
                        mealDetail.ingredients[index],
                      ),
                    ),
                  );
                },
                itemCount: mealDetail.ingredients.length,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              "Steps",
              style: TextStyle(
                fontSize: 25.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              width: 300.0,
              height: 150.0,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.grey),
              ),
              child: ListView.builder(
                itemBuilder: (context, index) {
                  return Column(
                    children: <Widget>[
                      ListTile(
                        leading: CircleAvatar(
                          backgroundColor: Colors.pink,
                          child: Text(
                            "#${index + 1}",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        title: Text(mealDetail.steps[index]),
                      ),
                      Divider(),
                    ],
                  );
                },
                itemCount: mealDetail.steps.length,
              ),
            ),
            Divider(),
          ],
        ),
      ),
    );
  }
}
