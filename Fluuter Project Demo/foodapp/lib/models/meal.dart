import 'package:flutter/foundation.dart';

enum Complexity {
  Simple,
  Challenging,
  Hard
}

enum Affordibility {
  Affordable,
  Pricely,
  Luxerious
}

class Meal {
  final String id;
  final List<String> categories;
  final String title;
  final String imageUrl;
  final int duration;
  final Complexity complexity;
  final Affordibility affordibility;
  final List<String> ingredients;
  final List<String> steps;
  final  isGlutenFree;
  final  isVegan;
  final  isVegetarian;
  final isLactoseFree;


  const Meal({
  this.id,
  this.categories,
  this.title,
  this.imageUrl,
  this.duration,
  this.complexity,
  this.affordibility,
  this.ingredients,
  this.steps,
  this.isGlutenFree,
  this.isVegan,
  this.isVegetarian,
  this.isLactoseFree,
});


}