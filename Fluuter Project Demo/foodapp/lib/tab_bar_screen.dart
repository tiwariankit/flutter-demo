import 'package:ems/dummy_data.dart';
import 'package:ems/filter.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import './category_screen.dart';
import './favorite.dart';
import 'models/meal.dart';

class TabBarScreen extends StatefulWidget {
  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  List<Meal> _availableMeal = DUMMY_MEALS;
  Map<String, bool> _fliter = {
    'glutin': false,
    'lactos': false,
    'vegan': false,
    'vegeterian': false
  };

  void _setFilter(Map<String, bool> filterData) {
    setState(() {
      _fliter = filterData;
      print(_fliter['glutin']);
      print(_fliter['lactos']);
      print(_fliter['vegan']);
      print(_fliter['vegeterian']);
      _availableMeal = DUMMY_MEALS.where((meal) {
        if (_fliter['glutin'] && !meal.isGlutenFree) {
          return false;
        }
        if (_fliter['lactos'] && !meal.isLactoseFree) {
          return false;
        }
        if (_fliter['vegan'] && !meal.isVegan) {
          return false;
        }
        if (_fliter['vegeterian'] && !meal.isVegetarian) {
          return false;
        }
        return true;
      }).toList();
    });
    print(_availableMeal.length);
  }
  Widget showToast(String msg) {
    Fluttertoast.showToast(
        msg: msg,
        fontSize: 28.0,
        gravity: ToastGravity.CENTER,
        toastLength: Toast.LENGTH_SHORT);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Scaffold(
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20.0),
                width: double.infinity,
                height: 130,
                decoration: BoxDecoration(
                  color: Colors.amber,
                ),
                alignment: Alignment.centerLeft,
                child: Text(
                  "Cooking Up!",
                  style: TextStyle(
                      color: Colors.pink,
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              ListTile(
                  leading: Icon(
                    Icons.restaurant,
                    size: 26.0,
                  ),
                  title: InkWell(
                    onTap: () => Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return TabBarScreen();
                    })),
                    child: Text(
                      "Meals",
                      style: TextStyle(
                          fontWeight: FontWeight.w900, fontSize: 24.0),
                    ),
                  )),
              ListTile(
                leading: Icon(
                  Icons.settings,
                  size: 26.0,
                ),
                title: InkWell(
                  onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => FilterScreen(_fliter,_setFilter))),
                  child: Text("Filter",
                      style: TextStyle(
                          fontWeight: FontWeight.w900, fontSize: 24.0)),
                ),
              )
            ],
          ),
        ),
        appBar: AppBar(
          title: Text("Deli Meals"),
          bottom: TabBar(
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.category),
                text: "Categories",
              ),
              Tab(
                icon: Icon(Icons.favorite),
                text: "Favorite",
              )
            ],
          ),
        ),
        body: TabBarView(
          children: [
            CategoryScreen(_availableMeal),
            Favorite(),
          ],
        ),
      ),
    );
  }
}
