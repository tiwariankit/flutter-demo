import 'package:flutter/cupertino.dart';
import './meal_detail_screen.dart';
import './meal_item.dart';
import './models/meal.dart';

import './dummy_data.dart';
import 'package:flutter/material.dart';
//import './category_screen.dart';

class CategoryMeal extends StatefulWidget {
  final String id;
  final String title;
  List<Meal> mealAvailable;
  CategoryMeal(this.id, this.title,this.mealAvailable);

  @override
  _CategoryMealState createState() => _CategoryMealState();
}

class _CategoryMealState extends State<CategoryMeal> {
  Complexity complexity;
  var load = false;

  @override
  void initState() {
    mealType();
    super.initState();
  }

  void _rmItem(String id) {
    setState(() {
      widget.mealAvailable.removeWhere((meal) => meal.id == id);
    });
  }
  List<Meal> mealType() {
    widget.mealAvailable=DUMMY_MEALS.where((test) => test.categories.contains(widget.id)).toList();
    return widget.mealAvailable;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      body: widget.mealAvailable == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          :ListView.builder(
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: ()=>Navigator.of(context).push(MaterialPageRoute(builder: (context)=>MealDetail(widget.mealAvailable[index].id))).then((result)=>_rmItem(result)),
                  child: MealItem(
                    title: widget.mealAvailable[index].title,
                    complexity: widget.mealAvailable[index].complexity,
                    duration: widget.mealAvailable[index].duration,
                    ImageUrl: widget.mealAvailable[index].imageUrl,
                    affordibility: widget.mealAvailable[index].affordibility,
                    removeItem: _rmItem,
                  ),
                );
              },
              itemCount: widget.mealAvailable.length,
            ),
    );
  }
}
