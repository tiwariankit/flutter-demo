import 'package:ems/tab_bar_screen.dart';
import 'package:flutter/material.dart';

class FilterScreen extends StatefulWidget {
  final Function saveFilter;
  final Map<String,bool> _currentFilter;

  FilterScreen(this._currentFilter,this.saveFilter);

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  bool glutin = false;
  bool lactos = false;
  bool vegeteian = false;
  bool vegan = false;

  @override
  void initState() {
    // TODO: implement initState
    glutin=widget._currentFilter['glutin'];
    lactos=widget._currentFilter['lactos'];
    vegeteian=widget._currentFilter['vegeterian'];
    vegan=widget._currentFilter['vegan'];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20.0),
              width: double.infinity,
              height: 130,
              decoration: BoxDecoration(
                color: Colors.amber,
              ),
              alignment: Alignment.centerLeft,
              child: Text(
                "Cooking Up!",
                style: TextStyle(
                    color: Colors.pink,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            ListTile(
                leading: Icon(
                  Icons.restaurant,
                  size: 26.0,
                ),
                title: InkWell(
                  onTap: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return TabBarScreen();
                  })),
                  child: Text(
                    "Meals",
                    style:
                        TextStyle(fontWeight: FontWeight.w900, fontSize: 24.0),
                  ),
                )),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text("Your Filter"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                setState(() {
                  final selectedFilter={
                    'glutin': glutin,
                    'lactos': lactos,
                    'vegan': vegan,
                    'vegeterian': vegeteian
                  };
                  widget.saveFilter(selectedFilter);
                });
              })
        ],
      ),
      body: Column(
        children: <Widget>[
          SizedBox(
            height: 15.0,
          ),
          Center(
              child: Text(
            "Adjust your meal selection.",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          )),
          SizedBox(
            height: 15.0,
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                SwitchListTile(
                    value: glutin,
                    onChanged: (newValue) {
                      setState(() {
                        glutin = newValue;
                      });
                    },
                    title: Text("Glutin free"),
                    subtitle: Text("only include glutin free meals")),
                SwitchListTile(
                    value: lactos,
                    onChanged: (newValue) {
                      setState(() {
                        lactos = newValue;
                      });
                    },
                    title: Text("Lactos free"),
                    subtitle: Text("only include Lactos free meals")),
                SwitchListTile(
                    value: vegeteian,
                    onChanged: (newValue) {
                      vegeteian = newValue;
                    },
                    title: Text("Vegeterian"),
                    subtitle: Text("only include vegeterian meals")),
                SwitchListTile(
                    value: vegan,
                    onChanged: (newValue) {
                      vegan = newValue;
                    },
                    title: Text("Vegan"),
                    subtitle: Text("only include veganmeals")),
              ],
            ),
          )
        ],
      ),
    );
  }
}
