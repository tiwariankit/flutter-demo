import 'package:ems/models/meal.dart';
import 'package:flutter/material.dart';
import './category_meals_screen.dart';
import './models/category_model.dart';

class CategoryScreen extends StatelessWidget {
  final List<Meal> getavailableMeal;

  CategoryScreen(this.getavailableMeal);

  @override
  Widget build(BuildContext context) {
    const data = const [
      Category(
        id: 'c1',
        title: 'Italian',
        color: Colors.purple,
      ),
      Category(
        id: 'c2',
        title: 'Quick & Easy',
        color: Colors.red,
      ),
      Category(
        id: 'c3',
        title: 'Hamburgers',
        color: Colors.orange,
      ),
      Category(
        id: 'c4',
        title: 'German',
        color: Colors.amber,
      ),
      Category(
        id: 'c5',
        title: 'Light & Lovely',
        color: Colors.blue,
      ),
      Category(
        id: 'c6',
        title: 'Exotic',
        color: Colors.green,
      ),
      Category(
        id: 'c7',
        title: 'Breakfast',
        color: Colors.lightBlue,
      ),
      Category(
        id: 'c8',
        title: 'Asian',
        color: Colors.lightGreen,
      ),
      Category(
        id: 'c9',
        title: 'French',
        color: Colors.pink,
      ),
      Category(
        id: 'c10',
        title: 'Summer',
        color: Colors.teal,
      ),
    ];

    return GridView(
      padding: const EdgeInsets.all(25.0),
      children: data.map((catData) {
        return InkWell(
          onTap: () {
            debugPrint(catData.id);
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return CategoryMeal(catData.id, catData.title,getavailableMeal);
            }));
          },
          child: Container(
            child: Text(
              catData.title,
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 18.0),
            ),
            padding: const EdgeInsets.all(15.0),
            decoration: BoxDecoration(
              color: catData.color,
              borderRadius: BorderRadius.circular(15.0),
            ),
          ),
        );
      }).toList(),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20.0,
          mainAxisSpacing: 20.0),
    );
  }
}
