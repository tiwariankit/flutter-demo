import 'package:ems/meal_detail_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './models/meal.dart';

class MealItem extends StatelessWidget {
  final String id;
  final String title;
  final int duration;
  final Complexity complexity;
  final Affordibility affordibility;
  final String ImageUrl;
  final Function removeItem;

  MealItem({this.id,
    this.title,
    this.duration,
    this.complexity,
    this.ImageUrl,
    this.affordibility,
    this.removeItem});

  String get ComplexityText {
    switch (complexity) {
      case Complexity.Simple:
        return "Simple";
        break;
      case Complexity.Challenging:
        return "Challenging";
        break;
      case Complexity.Hard:
        return "Hard";
        break;
      default:
        return "Unknwon";
    }
  }

  String get AffordibilityText {
    switch (affordibility) {
      case Affordibility.Affordable:
        return "Affordable";
        break;
      case Affordibility.Pricely:
        return "Pricely";
        break;
      case Affordibility.Luxerious:
        return "Luxerious";
        break;
      default:
        return "Unknwon";
    }
  }


  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0)),
      elevation: 6.0,
      margin: EdgeInsets.all(12.0),
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    topRight: Radius.circular(15.0)),
                child: Image.network(
                  ImageUrl,
                  height: 250,
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
              ),
              Positioned(
                bottom: 25.0,
                right: 10.0,
                child: Container(
                  padding: EdgeInsets.all(15.0),
                  width: 220,
                  color: Colors.black54,
                  child: Text(
                    title,
                    style: TextStyle(color: Colors.white, fontSize: 20.0),
                    overflow: TextOverflow.fade,
                  ),
                ),
              ),
            ],
          ),
          Padding(padding: EdgeInsets.only(top: 10.0)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(Icons.schedule),
                  Text("$duration min"),
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.work),
                  Text(ComplexityText),
                ],
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.attach_money),
                  Text(AffordibilityText),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
